package com.example.demo.advert;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Advert
{
    private int advertId;

    private String naziv;
    private String tipOglasa; // izdavanje ili prodaja
    private String tipObjekta; // garsonjera, jednosoban, dvosoban..
    private String grad;
    private String deoGrada;
    private String ulica;
    private Double brojSoba;
    private Double kvadratura;
    private String opis;
    private ArrayList<String> naziviGrejanja = new ArrayList<String>();
    private Double cena;
    private int idIzdavaca;

    private ArrayList<String> naziviSlikaOglasa;

    public Advert(int id, String naziv, String tipOglasa, String tipObjekta, String grad, String deoGrada, String ulica, Double brojSoba, Double kvadratura, String opis, ArrayList<String> naziviGrejanja, Double cena, int idIzdavaca, ArrayList<String> naziviSlikaOglasa) {
        this(id, naziv,tipOglasa, tipObjekta, grad, deoGrada, ulica, brojSoba, kvadratura, opis, naziviGrejanja, cena, idIzdavaca);
        this.naziviSlikaOglasa=naziviSlikaOglasa;
    }

    public Advert(String naziv, String tipOglasa, String tipObjekta, String grad, String deoGrada, String ulica, Double brojSoba, Double kvadratura, String opis, Double cena, ArrayList<String> naziviGrejanja) {
        this.naziv = naziv;
        this.tipOglasa = tipOglasa;
        this.tipObjekta = tipObjekta;
        this.grad = grad;
        this.deoGrada = deoGrada;
        this.ulica = ulica;
        this.brojSoba = brojSoba;
        this.kvadratura = kvadratura;
        this.opis = opis;
        this.naziviGrejanja = naziviGrejanja;
        this.cena = cena;
    }

    public Advert()
    {

    }

    public Advert(String naziv, String tipOglasa, String tipObjekta, String grad, String deoGrada, String ulica, Double brojSoba, Double kvadratura, String opis, ArrayList<String> naziviGrejanja, Double cena, int idIzdavaca) {

        this(naziv, tipOglasa, tipObjekta, grad, deoGrada, ulica, brojSoba, kvadratura, opis, cena, naziviGrejanja);
        this.idIzdavaca = idIzdavaca;
    }

    public Advert(int advertId, String naziv, String tipOglasa, String tipObjekta, String grad, String deoGrada, String ulica, Double brojSoba, Double kvadratura, String opis, ArrayList<String> naziviGrejanja, Double cena, int idIzdavaca)
    {
        this(naziv, tipOglasa, tipObjekta, grad, deoGrada, ulica, brojSoba, kvadratura, opis, naziviGrejanja, cena, idIzdavaca);
        this.advertId = advertId;
    }

    public Advert(String grad, String deoGrada)
    {
        this.grad = grad;
        this.deoGrada = deoGrada;
    }
    public int getAdvertId() { return advertId; }
    public String getOpis() {
        return opis;
    }

    public String getGrad() {
        return grad;
    }

    public String getTipOglasa() {
        return tipOglasa;
    }

    public void setTipOglasa(String tipOglasa) {
        this.tipOglasa = tipOglasa;
    }

    public String getTipObjekta() {
        return tipObjekta;
    }

    public void setTipObjekta(String tipObjekta) {
        this.tipObjekta = tipObjekta;
    }
    public Double getBrojSoba()
    {
        return brojSoba;
    }

    public Double getKvadratura() {
        return kvadratura;
    }


    public Double getCena() {
        return cena;
    }

    public String getNaziv()
    {
        return naziv;
    }
    public String getDeoGrada()
    {
        return deoGrada;
    }

    public String getUlica()
    {
        return ulica;
    }


    public int getIdIzdavaca()
    {
        return idIzdavaca;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public void setDeoGrada(String deoGrada) {
        this.deoGrada = deoGrada;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public void setBrojSoba(Double brojSoba) {
        this.brojSoba = brojSoba;
    }

    public void setKvadratura(Double kvadratura) {
        this.kvadratura = kvadratura;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }


    public void setCena(Double cena) {
        this.cena = cena;
    }

    public void setIdIzdavaca(int idIzdavaca) {
        this.idIzdavaca = idIzdavaca;
    }

    public List<String> getNaziviGrejanja() {
        return naziviGrejanja;
    }

    public void setNaziviGrejanja(ArrayList<String> naziviGrejanja) {
        this.naziviGrejanja = naziviGrejanja;
    }

    public ArrayList<String> getNaziviSlikaOglasa() {
        return naziviSlikaOglasa;
    }
}
