package com.example.demo.users;

import com.example.demo.advert.Advert;
import com.example.demo.db.DB;
import com.example.demo.notification.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
    @PostMapping(value = "/m2", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String addUser(@RequestBody User user) throws SQLException {
        System.out.println("PROSLO");
        System.out.println(user.getIme());
        System.out.println(user.getPrezime());
        System.out.println(user.getMail());
        System.out.println(user.getSifra());
        System.out.println(user.getBrTelefona());
        System.out.println(user.getBrojOglasa());
        System.out.println(user.getProsecnaOcena());

        DB db = new DB();
        boolean imaLi = db.getUserMailFromDB(user.getMail());

        if (imaLi == false) {
            System.out.println("Postoji takav u bazi");
            return "postoji";
        } else {
            System.out.println("Ne postoji takav u bazi");

            String provera = db.insertUserInDB(user);

            if (provera != "Inserted")
                return "greska";

            return "uspesno";
        }
    }

    @PostMapping(value = "/m3", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public int loginUser(@RequestBody User user) throws SQLException {
        System.out.println("PROSLO");
        System.out.println(user.getIme());
        System.out.println(user.getPrezime());
        System.out.println(user.getMail());
        System.out.println(user.getSifra());
        System.out.println(user.getBrTelefona());
        System.out.println(user.getBrojOglasa());
        System.out.println(user.getProsecnaOcena());
        System.out.println(user.getSlika());

        DB db = new DB();
        User userLogin = db.checkLogin(user.getMail());

        if (userLogin == null) {
            System.out.println("Ne moze login");
            return -2; //"nijeRegistrovan";
        } else {
            System.out.println("Moze login");

            if (user.getSifra().equals(userLogin.getSifra())) {
                if (user.getMail().equals("admin@gmail.com")) {
                    return -1; //"admin";
                } else {
                    return userLogin.getId();//"uspesno";
                }
            }
            return 0; //"pogresnaSifra";
        }
    }

    @PostMapping(value = "/userInformation", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public User getAllNotificationForOwnerByID(@RequestBody User user)
    {
        DB db = new DB();
        return db.getAllUserInformationFromDB(user.getId());
    }

    @PostMapping(value = "/updateUser", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String updateUserInDB(@RequestBody User user) {
        DB db = new DB();
        db.updateUser(user);
        return "uspesno";
    }

    @GetMapping("/m4")
    public List<User> getAllUsers() {
        DB db = new DB();
        return db.getAllUsersFromDB();
    }

    @PostMapping(value = "/obrisi", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String deleteUserInDB(@RequestBody User user) throws SQLException {
        System.out.println(user.getMail());
        DB db = new DB();
        boolean provera = db.deleteUserInDB(user.getMail());
        if (provera)
            return "uspesno";
        return "neuspesno";
    }

    @Autowired
    ServletContext context;

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("mail") String mail) throws SQLException {
        try {
            DB db = new DB();
            String fileName = file.getOriginalFilename();
            db.insertPictureInDB(fileName, mail);
            byte[] bytes = file.getBytes();
            Path currentWorkingDir = Paths.get("").toAbsolutePath();
            System.out.println(currentWorkingDir.normalize().toString());
            File fajlSlike = new File("Front/assets/img/slikeKorisnika");
            boolean pr1 = fajlSlike.mkdir();
            if(pr1==true)
                System.out.println("Napravljen za slike je fajl");
            File fajl = new File("Front/assets/img/slikeKorisnika/"+mail);
            boolean pr2 = fajl.mkdir();
            if(pr2==true)
                System.out.println("Napravljen fajl");
            else
                System.out.println("Nije napravljen fajl");
            Path path = Paths.get( currentWorkingDir+ "\\Front\\assets\\img\\slikeKorisnika\\"+mail+"\\"+ file.getOriginalFilename());
            System.out.println(path);
            Files.write(path, bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "neuspesno";
        }
        return "uspesno";

    }

    @PostMapping(value = "/viewing",  consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String shedulingViewing (@RequestBody Notification notification)
    {
        DB db = new DB();


        int visitorId = notification.getUserId();
        int advertId = notification.getAdverId();
        int ownerId = db.getOwnerId(advertId);
        String time = notification.getTime();
        db.insertNotification(advertId,visitorId,ownerId);

        db.insertViewing(visitorId,advertId,time,db.useLastNotificationId());

        return "uspesno";


    }

    @PostMapping("/access")
    public String accessAnotherUser (@RequestParam("user") String user,@RequestParam("owner") String owner)
    {
        DB db = new DB();
        System.out.println(user);
        System.out.println(owner);
        int userId = db.getIdUserFromDB(user);
        int ownerId = db.getIdUserFromDB(owner);
        System.out.println("Ownerid - "+ownerId);
        System.out.println("userId - "+userId);

        if(db.notificationExistence(userId, ownerId)) {
            return "moze";
        }
        return "neMoze";
    }

    @PostMapping(value = "/myInfo",  consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public User getMyInfo (@RequestBody User user)
    {
        DB db = new DB();
        return db.getUserFromDB(user.getMail());
    }

    @PostMapping("/likes")
    public String userLike(@RequestParam("user") String user,@RequestParam("advertId") int advertId)
    {
        DB db = new DB();
        int userId = db.getIdUserFromDB(user);
        return db.like(userId, advertId);
    }

    @PostMapping("/isliked")
    public String isUserLiked(@RequestParam("user") String user,@RequestParam("advertId") int advertId)
    {
        DB db = new DB();
        int userId = db.getIdUserFromDB(user);
        return db.isLiked(userId, advertId);
    }

    @PostMapping("/numberoflikes")
    public int numberOfLikes(@RequestParam("advertId") int advertId)
    {
        DB db = new DB();
        return db.numberOfLikesAdvert(advertId);
    }


}
