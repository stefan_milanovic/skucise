package com.example.demo.notification;

import com.example.demo.db.DB;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Notification {

    private int id;
    private int advertId;
    private int userId;
    private int ownerId;
    private String response;

    //  bogdan
    private String nazivOglasa;
    private String imeZahtevaoca;
    private String prezime;

    private ArrayList<String> naziviSlikaOglasa;

    private String time;

    public Notification(int id, int advertId, int userId, int ownerId, String response)
    {
        this.id = id;
        this.advertId = advertId;
        this.userId = userId;
        this.ownerId = ownerId;
        this.response = response;
    }

    public Notification(int id,String nazivOglasa,String time, String imeZahtevaoca,String prezime,String response, int advertId, int userId)
    {
        this.id = id;
        this.nazivOglasa = nazivOglasa;
        this.time = time;
        this.imeZahtevaoca = imeZahtevaoca;
        this.prezime = prezime;
        this.response = response;
        this.advertId = advertId;
        this.userId = userId;
        DB db = new DB();
        this.naziviSlikaOglasa = db.getPicturesFromDB(advertId);

    }

    public Notification(int userId, int advertId, String time)
    {
        this.userId = userId;
        this.advertId = advertId;
        this.time = time;
    }

    public Notification(){}

    public int getAdvertId() {
        return advertId;
    }

    public String getNazivOglasa() {
        return nazivOglasa;
    }

    public String getImeZahtevaoca() {
        return imeZahtevaoca;
    }

    public String getPrezime() {
        return prezime;
    }

    public int getId() {
        return id;
    }

    public int getAdverId() {
        return advertId;
    }

    public void setAdvertId(int advertId) {
        this.advertId = advertId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getTime() {
        return time;
    }

    public ArrayList<String> getNaziviSlikaOglasa() {
        return naziviSlikaOglasa;
    }
}
