package com.example.demo.advert;

import com.example.demo.db.DB;
import com.example.demo.filter.Filter;
import com.example.demo.users.User;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/adverts")
public class AdvertController
{
    @GetMapping("/m1")
    public List<Advert> getAllAdverts()
    {
        DB db = new DB();
        return db.getAdvertsFromDB();
    }

    @PostMapping(value="/m10", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public int addAdvert(@RequestBody Advert advert) throws SQLException {
        System.out.println("PROSLO - OGLAS");
        System.out.println(advert.getNaziv());
        System.out.println(advert.getTipOglasa());
        System.out.println(advert.getTipObjekta());
        System.out.println(advert.getGrad());
        System.out.println(advert.getDeoGrada());
        System.out.println(advert.getUlica());
        System.out.println(advert.getBrojSoba());
        System.out.println(advert.getKvadratura());
        System.out.println(advert.getOpis());
        System.out.println(advert.getCena());
        System.out.println("idIzdavaca: "+advert.getIdIzdavaca());


        for (String g : advert.getNaziviGrejanja()) {
            System.out.println(g);
        }

        DB db = new DB();
        int idOglasa = db.insertAdvertInDB(advert);
        System.out.println("                                idOglasa="+idOglasa);
        boolean provera = db.insertIntoOglasGrejanje(db.getIdGrejanja(advert.getNaziviGrejanja()), idOglasa);

        if(provera)
            System.out.println("Upisano u oglasgrejanje");

        System.out.println("                                idOglasa="+idOglasa);
        if (idOglasa > 0)
            return idOglasa; //"uspesno"
        return -1; //"greska";

    }

    @PostMapping("/advertDBDelete")
    public String deleteAdvertInDB(@RequestParam("advertId") int advert) throws SQLException
    {
        DB db = new DB();
        db.deleteAdvertInDB(advert);
        return "uspesno";
    }

    @GetMapping("/cities")
    public List<Advert> getAllCities()
    {
        DB db = new DB();
        return db.getCitiesFromDB();
    }

    @GetMapping("/heating")
    public List<String> getAllHeating()
    {
        DB db = new DB();
        return db.getAllHeatingFromDB();
    }

    @PostMapping(value = "/myadvert",  consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Advert> getAllMyAdvert (@RequestBody User user)
    {
        DB db = new DB();
        return db.getAdvertsFromDBFromUser(db.getIdUserFromDB(user.getMail()));
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("files") MultipartFile file, @RequestParam("idOglasa") int idOglasa) throws SQLException {
        System.out.println("USLO");
        try {
            DB db = new DB();
            String fileName = file.getOriginalFilename();
            boolean provera = db.insertPictureInSlikeOglasa(fileName, idOglasa);
            System.out.println(provera+" "+fileName+" "+idOglasa);
            byte[] bytes = file.getBytes();
            Path currentWorkingDir = Paths.get("").toAbsolutePath();
            System.out.println(currentWorkingDir.normalize().toString());
            File fajlSlike = new File("Front/assets/img/slikeOglasa");
            boolean pr1 = fajlSlike.mkdir();
            if(pr1==true)
                System.out.println("Napravljen za slike je fajl");
            File fajl = new File("Front/assets/img/slikeOglasa/"+idOglasa);
            boolean pr2 = fajl.mkdir();
            if(pr2==true)
                System.out.println("Napravljen fajl");
            else
                System.out.println("Nije napravljen fajl");
            Path path = Paths.get( currentWorkingDir+ "\\Front\\assets\\img\\slikeOglasa\\"+idOglasa+"\\"+ file.getOriginalFilename());
            System.out.println(path);
            Files.write(path, bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "neuspesno";
        }
        return "uspesno";

    }
}
