package com.example.demo.filter;

import com.example.demo.advert.Advert;
import com.example.demo.db.DB;
import org.attoparser.dom.INestableNode;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/filter")
public class FilterController
{
    @PostMapping(value="/f1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ArrayList<Filter> filtriranje(@RequestBody Filter filter) throws SQLException
    {
        System.out.println("Uslo u filter");
        DB db = new DB();
        ArrayList<Integer> listaIdGrejanja=new ArrayList<Integer>();
        if(filter.getGrejanje()!=null)
            listaIdGrejanja = db.getIdGrejanjaFromDB(filter.getGrejanje());

        ArrayList<Filter> idjeviModel = db.filterGetAdvertsFromDB(filter, listaIdGrejanja);
        return idjeviModel;
    }
}
