package com.example.demo.db;

import com.example.demo.advert.Advert;
import com.example.demo.filter.Filter;
import com.example.demo.notification.Notification;
import com.example.demo.users.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DB {
    Connection connection = null;
    Statement statement = null;
    String sql = null;

    public DB() {
        System.out.println("Konektuj se");
        try {
            connection = DriverManager.getConnection("jdbc:mariadb://localhost/skuci_se", "root", "");
            System.out.println("Konektovali smo se");
            statement = connection.createStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean getUserMailFromDB(String mail) {
        sql = "SELECT * FROM korisnik WHERE email='" + mail + "'";
        System.out.println(sql);
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return true;
            return false;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User getAllUserInformationFromDB(int id) {
        sql = "SELECT * FROM korisnik WHERE id=" + id;
        System.out.println(sql);
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;
            return new User(resultSet.getInt("id"), resultSet.getString("ime"), resultSet.getString("prezime"), resultSet.getString("slika"), resultSet.getString("brTelefona"), resultSet.getString("email"), resultSet.getString("sifra"), resultSet.getInt("brojOglasa"), resultSet.getInt("prosecnaOcena"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User getUserFromDB(String mail) {
        sql = "SELECT id, ime, prezime, slika, brTelefona, email, AES_DECRYPT(sifra , '2&!4mvUY#LiA973M7JXA') as 'sifra', brojOGlasa, prosecnaOcena  FROM korisnik WHERE email='" + mail + "'";
        System.out.println(sql);
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;
            return new User(resultSet.getString("ime"), resultSet.getString("prezime"), resultSet.getString("slika"), resultSet.getString("brTelefona"), mail, resultSet.getString("sifra"), resultSet.getInt("brojOglasa"), resultSet.getDouble("prosecnaOcena"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public User checkLogin(String mail) {
        sql = "SELECT id, ime, prezime, slika, brTelefona, email, AES_DECRYPT(sifra , '2&!4mvUY#LiA973M7JXA') as 'sifra', brojOGlasa, prosecnaOcena  FROM korisnik WHERE email='" + mail + "'";

        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;
            return new User(resultSet.getInt("id"), resultSet.getString("ime"), resultSet.getString("slika"), resultSet.getString("prezime"), resultSet.getString("brTelefona"), mail, resultSet.getString("sifra"), resultSet.getInt("brojOglasa"), resultSet.getDouble("prosecnaOcena"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean insertPictureInDB(String fileName, String mail) throws SQLException {
        sql = "UPDATE korisnik SET slika='"+fileName+"' WHERE email='"+mail+"'";
        boolean checking = statement.execute(sql);
        if (!checking) {
            return true;
        }
        return false;
    }

    public String insertUserInDB(User user) throws SQLException {
        System.out.println("Inserting row...");
        sql = "INSERT INTO korisnik"
                + "(ime, prezime, brTelefona, email, sifra, brojOglasa, prosecnaOcena)"
                + "VALUES ('" + user.getIme() + "', '" + user.getPrezime() + "', '" + user.getBrTelefona() + "', '" + user.getMail() + "',  AES_ENCRYPT('" + user.getSifra() + "', '2&!4mvUY#LiA973M7JXA'), '" + user.getBrojOglasa() + "', '" + user.getProsecnaOcena() + "')";
        boolean checking = statement.execute(sql);
        if (!checking) {
            System.out.println("Row inserted");
            return "Inserted";
        }
        System.out.println("Row not inserted");
        return "Not inserted";
    }

    public boolean deleteUserInDB(String email) throws SQLException {
        System.out.println("Deleting row...");


        int id = getIdUserFromDB(email);
        deleteLikeFromUser(id);
        deleteViewingFromUser(id);
        deleteNotificationFromUser(id);

        sql = "select id from oglas where idIzdavaca=" + id;
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
           deleteAdvertInDB(resultSet.getInt("id"));
        }

        sql = "DELETE from korisnik where email='" + email + "'";
        boolean checking = statement.execute(sql);
        if (!checking) {
            System.out.println("Row deleted");
            return true;
        }
        System.out.println("Row not deleted");
        return false;
    }

    public List<Advert> getAdvertsFromDB() {
        sql = "SELECT * FROM oglas";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            ArrayList<String> naziviSlikaOglasa = getPicturesFromDB(resultSet.getInt("id"));

            Advert advert = new Advert(resultSet.getInt("id"), resultSet.getString("naziv"), resultSet.getString("tipOglasa"), resultSet.getString("tipObjekta"), resultSet.getString("grad"), resultSet.getString("deoGrada"), resultSet.getString("ulica"), resultSet.getDouble("brojSoba"), resultSet.getDouble("kvadratura"), resultSet.getString("opis"), getGrejanjaZaOglas(resultSet.getInt("id")), resultSet.getDouble("cena"), resultSet.getInt("idIzdavaca"), naziviSlikaOglasa);
            ArrayList<Advert> adverts = new ArrayList<Advert>();
            adverts.add(advert);

            while (resultSet.next()) {
                naziviSlikaOglasa = getPicturesFromDB(resultSet.getInt("id"));
                advert = new Advert(resultSet.getInt("id"), resultSet.getString("naziv"), resultSet.getString("tipOglasa"), resultSet.getString("tipObjekta"), resultSet.getString("grad"), resultSet.getString("deoGrada"), resultSet.getString("ulica"), resultSet.getDouble("brojSoba"), resultSet.getDouble("kvadratura"), resultSet.getString("opis"), getGrejanjaZaOglas(resultSet.getInt("id")), resultSet.getDouble("cena"), resultSet.getInt("idIzdavaca"), naziviSlikaOglasa);
                adverts.add(advert);
            }
            return adverts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // za filter
    public ArrayList<Filter> filterGetAdvertsFromDB(Filter filter, ArrayList<Integer> listaIdGrejanja)
    {
        //ArrayList<Integer> oglasiIdFilterLista = new ArrayList<Integer>();
        ArrayList<Filter> oglasiIdFilterLista = new ArrayList<Filter>();
        sql = "ALTER VIEW prvi AS SELECT DISTINCT o.id FROM oglas AS o JOIN oglasgrejanje AS og ON o.id=og.idOglasa";
        //sql = "SELECT * FROM oglas AS o WHERE o.grad='"+filter.getGrad()+"' AND o.cena<="+filter.getMaxCena()+" AND o.cena>="+filter.getMinCena()+" AND o.kvadratura<="+filter.getMaxKvadratura()+" AND o.kvadratura>="+filter.getMinKvadratura()+" AND o.brojSoba<="+filter.getMaxSoba()+" AND o.brojSoba>="+filter.getMinSoba()+" AND o.tipOglasa='"+filter.getTipOglasa()+"' AND o.ulica='"+filter.getOkvirnaLokacija()+"'";
        int checker = 0; // kada je 0 onda se pise WHERE

        if(filter.getGrad() != null) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
//            if(checker > 0)
//                sql += " AND ";
            sql += "o.grad='" + filter.getGrad() + "'";
        }
        if(filter.getMaxCena() != 0) {

            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.cena<=" + filter.getMaxCena();
        }
        if(filter.getMinCena() != 0)
        {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.cena>="+filter.getMinCena();
        }
        if(filter.getMaxKvadratura() != 0) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.kvadratura<=" + filter.getMaxKvadratura();
        }
        if(filter.getMinKvadratura() != 0) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.kvadratura>=" + filter.getMinKvadratura();
        }
        if(filter.getMaxSoba() != 0 && filter.getMaxSoba()!=6) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.brojSoba<=" + filter.getMaxSoba();
        }
        if(filter.getMinSoba() != 0) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            if(filter.getMinSoba()==6)
                sql += "o.brojSoba>" + 5;
            else
                sql += "o.brojSoba>=" + filter.getMinSoba();
        }
        if(filter.getTipOglasa() != null) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.tipOglasa='" + filter.getTipOglasa() + "'";
        }

        if(filter.getOkvirnaLokacija() != null) {
            if(checker > 0)
                sql += " AND ";
            if (checker == 0){
                sql += " WHERE ";
                checker = 1;
            }
            sql += "o.deoGrada='" + filter.getOkvirnaLokacija() + "'";
        }
//      REZUltat ovog gore spoj sa ovim dole
        String sql2="";
        if(filter.getTipObjekta()!=null || filter.getGrejanje()!=null)
        {
//            sql2+="CREATE VIEW objekatGrejanje AS";
            sql2+=" ALTER VIEW druga AS SELECT DISTINCT o.id ";
            sql2+=" from oglas o JOIN oglasgrejanje og on o.id=og.idOglasa join grejanje g on og.idGrejanja=g.id ";
            //sql2+=" WHERE (o.tipObjekta='kuca' OR o.tipObjekta='stan') and (g.naziv='Gas' OR g.naziv='Podno');"

            if(filter.getTipObjekta()!=null || filter.getGrejanje()!=null)
                sql2+=" WHERE (";

            int ch=0;
            int ch2=0;
            if(filter.getTipObjekta()!=null)
            {
                for (String tobjekta : filter.getTipObjekta())
                {
                    if(ch!=0)
                        sql2+=" OR ";
                    else
                        ch=1;

                    if(tobjekta != null) {
                        sql2 += "o.tipObjekta='" + tobjekta + "'";
                        ch2=1;
                    }
                }
                sql2+=")";
            }

            ch=0;
            if(filter.getGrejanje()!=null)
            {
                if(ch2==1)
                    sql2+=" and (";
                for(String tgrejanje : filter.getGrejanje())
                {
                    if(ch!=0)
                        sql2+=" OR ";
                    else
                        ch=1;

                    if(tgrejanje != null) {
                        sql2 += "g.naziv='" + tgrejanje + "'";
                    }
                }
                sql2+=")";
            }

        }

        try {
            int ch3=0;
            if(sql2.length()>0)
            {
                System.out.println("DRUGI"+sql2);
                ResultSet resultSet2 = statement.executeQuery(sql2);
                System.out.println("DRUGI"+sql2);
            }
            else
                ch3=1;
            System.out.println("PRVI"+sql);
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println("PRVI"+sql);

            if(ch3==0)
            {
                String sql3 = "SELECT * FROM prvi AS p JOIN druga as d on p.id=d.id";
                ResultSet resultSet3 = statement.executeQuery(sql3);
                System.out.println("\n DOVDESTIGO ako postoji \n");
                if (!resultSet3.first())
                    return null;

                //oglasiIdFilterLista.add(resultSet3.getInt("id"));
                oglasiIdFilterLista.add(new Filter(resultSet3.getInt("id")));
                while (resultSet3.next()) {
                    //oglasiIdFilterLista.add(resultSet3.getInt("id"));
                    oglasiIdFilterLista.add(new Filter(resultSet3.getInt("id")));
                }
                sql3="DROP VIEW prvi";
//                statement.executeQuery(sql3);
                sql3="DROP VIEW druga";
//                statement.executeQuery(sql3);
            }
            else
            {
                System.out.println("\n DOVDESTIGO iznad \n");
                sql="SELECT * FROM prvi";
                resultSet = statement.executeQuery(sql);
                if(!resultSet.first())
                    return null;
                System.out.println("\n DOVDESTIGO sredina \n");
                //oglasiIdFilterLista.add(resultSet.getInt("id"));
                oglasiIdFilterLista.add(new Filter(resultSet.getInt("id")));
                while (resultSet.next()) {
                    //oglasiIdFilterLista.add(resultSet.getInt("id"));
                    oglasiIdFilterLista.add(new Filter(resultSet.getInt("id")));
                }
                System.out.println("\n DOVDESTIGO ispod \n");
                sql="DROP VIEW prvi";
//                statement.executeQuery(sql);
            }

            return oglasiIdFilterLista;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<String> getPicturesFromDB(int idOglasa)
    {
        sql = "SELECT * FROM slikeoglasa WHERE idOglasa="+idOglasa;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            ArrayList<String> naziviSlikaOglasa = new ArrayList<String>();
            naziviSlikaOglasa.add(resultSet.getString("slika"));

            while (resultSet.next()) {
                naziviSlikaOglasa.add(resultSet.getString("slika"));
            }
            return naziviSlikaOglasa;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<String> getGrejanjaZaOglas(int idOglasa)
    {
        sql = "SELECT grejanje.naziv FROM oglasgrejanje JOIN grejanje ON oglasgrejanje.idGrejanja=grejanje.id WHERE idOglasa="+idOglasa;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            String grejanjeNaziv = resultSet.getString("grejanje.naziv");
            ArrayList<String> grejanjeNazivi = new ArrayList<String>();
            grejanjeNazivi.add(grejanjeNaziv);

            while (resultSet.next()) {
                grejanjeNaziv = resultSet.getString("grejanje.naziv");
                grejanjeNazivi.add(grejanjeNaziv);
            }
            return grejanjeNazivi; // vraca listu (nazivGrejanja) za 1 oglas
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public Advert getAdvertFromDB(int id) {
        sql = "SELECT * FROM oglas WHERE id=" + id;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;
            return new Advert(resultSet.getString("naziv"), resultSet.getString("tipOglasa"), resultSet.getString("tipObjekta"), resultSet.getString("grad"), resultSet.getString("deoGrada"), resultSet.getString("ulica"), resultSet.getDouble("brojSoba"), resultSet.getDouble("kvadratura"), resultSet.getString("opis"), getGrejanjaZaOglas(id), resultSet.getDouble("cena"), resultSet.getInt("idIzdavaca"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // doraditi
    public int  insertAdvertInDB(Advert advert) throws SQLException {
        System.out.println("Inserting row...");
        sql = "INSERT INTO oglas"
                + "(naziv, tipOglasa, tipObjekta, grad, deoGrada, ulica, brojSoba, kvadratura, opis, cena, idIzdavaca)"
                + "VALUES ('" + advert.getNaziv() + "', '" + advert.getTipOglasa() + "', '" + advert.getTipObjekta() + "', '" + advert.getGrad() + "', '" + advert.getDeoGrada() + "', '" + advert.getUlica() + "', '" + advert.getBrojSoba() + "', '" + advert.getKvadratura() + "', '" + advert.getOpis() + "', '" + advert.getCena() + "', '" + advert.getIdIzdavaca() + "')";
        boolean checking = statement.execute(sql);
        if (!checking) {
            System.out.println("Row inserted "+getLastInsertedIdAdvert());
            return getLastInsertedIdAdvert();//"Inserted";
        }
        System.out.println("Row not inserted");
        return -1;//"Not inserted";
    }
    public int getLastInsertedIdAdvert() throws SQLException {
        sql = "SELECT id FROM oglas ORDER BY id DESC LIMIT 1;";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return -1;
            return resultSet.getInt("id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<Integer> getIdGrejanja(List<String> naziviGrejanja)
    {
        ArrayList<Integer> listaIdGrejanja = new ArrayList<Integer>();
        for (String nazivGrejanja : naziviGrejanja) {
            sql = "SELECT id FROM grejanje WHERE naziv='"+nazivGrejanja+"'";
            try {
                ResultSet resultSet = statement.executeQuery(sql);
                if(!resultSet.first())
                    return null;

                listaIdGrejanja.add(resultSet.getInt("id"));
                System.out.println("selektovano: " + resultSet.getInt("id"));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return listaIdGrejanja;
    }

    public boolean insertIntoOglasGrejanje(ArrayList<Integer> listaIdGrejanja, int idOglasa) throws SQLException {
        for (int idGrejanja : listaIdGrejanja) {
            sql = "INSERT INTO oglasgrejanje (idOglasa, idGrejanja) VALUES (" + idOglasa + ", " + idGrejanja + ")";
            boolean checking = statement.execute(sql);
            if(!checking)
                System.out.println("Row inserted");
        }
        return true;
    }

    public boolean insertPictureInSlikeOglasa(String fileName, int idOglasa) throws SQLException {
        sql = "INSERT INTO slikeoglasa (slika, idOglasa) VALUES ('" + fileName + "', " + idOglasa + ")";
        boolean checking = statement.execute(sql);
        if (!checking)
        {
            System.out.println("Row inserted");
            return true;
        }

        System.out.println("Row not inserted");
        return false;
    }


    public boolean deleteAdvertInDB(int id){
        try {
            System.out.println("Deleting row...");
            deletePicturesOfAdvert(id);
            deleteHeatAdvert(id);
            deleteLikesFromAdvert(id);
            deleteViewingFromAdvert(id);
            deleteNotificationFromAdvert(id);
            sql = "DELETE from oglas where id=" + id;

            boolean checking = statement.execute(sql);
            if (!checking) {
                System.out.println("Row deleted");
                return true;
            }
            System.out.println("Row not deleted");
            return false;
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean insertViewing(int visitorId, int advertId, String time, int notificationId) {
        try {
            System.out.println("Add viewing...");
            sql = "INSERT INTO `RazgledanjeStana` (`vreme`, `idOglasa`, `idKorisnika`,`idObavestenja`) VALUES ('" + time + "', '" + advertId + "', '" + visitorId + "', '" + notificationId + "');";
            return statement.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // za filter
    public ArrayList<Integer> getIdGrejanjaFromDB(ArrayList<String> naziviGrejanja) {
        ArrayList<Integer> listaIdGrejanja = new ArrayList<Integer>();
        for (String nazivGrejanja : naziviGrejanja) {
            sql = "SELECT id FROM grejanje WHERE naziv='" + nazivGrejanja + "'";
            try {
                ResultSet resultSet = statement.executeQuery(sql);
                System.out.println("s1adasdasdasdsadsadasd"+listaIdGrejanja.size());
                if (!resultSet.first())
                    return null;
                System.out.println("2sadasdasdasdsadsadasd"+listaIdGrejanja.size());
                System.out.println(resultSet.getInt("id"));

                listaIdGrejanja.add(resultSet.getInt("id"));
                System.out.println("sadasdasdasdsadsadasd"+listaIdGrejanja.size());
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return listaIdGrejanja;
    }

    public int getIdUserFromDB(String email) {
        try {
        sql = "SELECT id FROM korisnik WHERE email='"+email+"'";
            ResultSet resultSet = statement.executeQuery(sql);

            if (!resultSet.first()) {

                return -1;
            }
            System.out.println(resultSet.getInt("id"));


            return resultSet.getInt("id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Advert> getCitiesFromDB() {
        sql = "SELECT grad, deoGrada FROM oglas";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            Advert advert = new Advert(resultSet.getString("grad"), resultSet.getString("deoGrada"));
            ArrayList<Advert> adverts = new ArrayList<Advert>();
            adverts.add(advert);

            while (resultSet.next()) {
                advert = new Advert(resultSet.getString("grad"), resultSet.getString("deoGrada"));
                adverts.add(advert);
            }
            return adverts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<User> getAllUsersFromDB() {
        sql = "SELECT ime, prezime, slika, brTelefona, email, AES_DECRYPT(sifra , '2&!4mvUY#LiA973M7JXA') as 'sifra', brojOGlasa, prosecnaOcena FROM korisnik";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            User user = new User(resultSet.getString("ime"), resultSet.getString("prezime"), resultSet.getString("slika"), resultSet.getString("brTelefona"), resultSet.getString("email"), resultSet.getString("sifra"), resultSet.getInt("brojOglasa"), resultSet.getDouble("prosecnaOcena"));
            ArrayList<User> users = new ArrayList<User>();
            users.add(user);

            while (resultSet.next()) {
                user = new User(resultSet.getString("ime"), resultSet.getString("prezime"), resultSet.getString("slika"), resultSet.getString("brTelefona"), resultSet.getString("email"), resultSet.getString("sifra"), resultSet.getInt("brojOglasa"), resultSet.getDouble("prosecnaOcena"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<String> getAllHeatingFromDB() {
        sql = "SELECT naziv FROM `grejanje`";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            String heat = resultSet.getString("naziv");
            ArrayList<String> heatList = new ArrayList<String>();
            heatList.add(heat);

            while (resultSet.next()) {
                heat = resultSet.getString("naziv");
                heatList.add(heat);
            }
            return heatList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Notification> getAllNotificationForOwner(int ownerId) {
        try {
            sql= "SELECT ob.id, k.ime,k.prezime,r.vreme,o.naziv,ob.odgovor,o.id,k.id  " +
                    "FROM obavestenja AS ob JOIN razgledanjestana AS r ON ob.id=r.idObavestenja " +
                    "                    JOIN oglas AS o ON ob.idOglasa=o.id" +
                    "                    JOIN korisnik AS k ON ob.idKorisnika = k.id" +
                    "                    WHERE ob.idVlasnika = "+ownerId;

            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            Notification notification = new Notification(resultSet.getInt("ob.id"), resultSet.getString("o.naziv"),resultSet.getString("r.vreme"),resultSet.getString("k.ime"),resultSet.getString("k.prezime"), resultSet.getString("ob.odgovor"), resultSet.getInt("o.id"), resultSet.getInt("k.id"));
            ArrayList<Notification> notifications = new ArrayList<Notification>();
            notifications.add(notification);

            while (resultSet.next()) {
                notification = new Notification(resultSet.getInt("ob.id"), resultSet.getString("o.naziv"),resultSet.getString("r.vreme"),resultSet.getString("k.ime"),resultSet.getString("k.prezime"), resultSet.getString("ob.odgovor"), resultSet.getInt("o.id"), resultSet.getInt("k.id"));
                notifications.add(notification);
            }

            return notifications;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Notification> getAllNotificationForUser(int userId) {
        try {
            sql= "SELECT ob.id, k.ime,k.prezime,r.vreme,o.naziv,ob.odgovor,o.id,k.id " +
                    "FROM obavestenja AS ob JOIN razgledanjestana AS r ON ob.id=r.idObavestenja " +
                    "                    JOIN oglas AS o ON ob.idOglasa=o.id" +
                    "                    JOIN korisnik AS k ON ob.idKorisnika = k.id" +
                    "                    WHERE ob.idKorisnika = "+userId+" and odgovor is not null ";

            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            Notification notification = new Notification(resultSet.getInt("ob.id"), resultSet.getString("o.naziv"),resultSet.getString("r.vreme"),resultSet.getString("k.ime"),resultSet.getString("k.prezime"), resultSet.getString("ob.odgovor"), resultSet.getInt("o.id"), resultSet.getInt("k.id"));
            ArrayList<Notification> notifications = new ArrayList<Notification>();
            notifications.add(notification);

            while (resultSet.next()) {
                notification = new Notification(resultSet.getInt("ob.id"), resultSet.getString("o.naziv"),resultSet.getString("r.vreme"),resultSet.getString("k.ime"),resultSet.getString("k.prezime"), resultSet.getString("ob.odgovor"), resultSet.getInt("o.id"), resultSet.getInt("k.id"));
                notifications.add(notification);
            }

            return notifications;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean confirmViewing(int notificationId, int advertId, int userId) {
        try {
            sql = "UPDATE `obavestenja` SET odgovor = 'DA' WHERE id=" + notificationId;
            statement.execute(sql);
            sql = "UPDATE `razgledanjeStana` SET potvrda = 'DA' WHERE idOglasa=" + advertId + " and idKorisnika=" + userId;
            System.out.println("Confirming viewing...");
            return statement.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean cancleViewing(int notificationId, int advertId, int userId) {
        try {
            sql = "UPDATE `obavestenja` SET odgovor = 'NE' WHERE id=" + notificationId;
            statement.execute(sql);
            sql = "UPDATE `razgledanjeStana` SET potvrda = 'NE' WHERE idOglasa=" + advertId + " and idKorisnika=" + userId;
            System.out.println("Canceling viewing...");
            return statement.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean insertNotification(int advertId, int userId, int ownerId) {
        try {
            System.out.println("Add notification...");
            sql = "INSERT INTO `Obavestenja` (`idOglasa`, `idKorisnika`, `idVlasnika`) VALUES('" + advertId + "', '" + userId + "', '" + ownerId + "');";
            return statement.execute(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int useLastNotificationId()
    {
        try {
            System.out.println("Add notification...");
            sql = "SELECT id FROM obavestenja ORDER BY id DESC LIMIT 1";

            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return -1;
            return  resultSet.getInt("id");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean deleteTheNotification(int notificationId) {
        try {
            sql = "DELETE FROM `obavestenja` WHERE id=" + notificationId;
            statement.execute(sql);
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public boolean notificationExistence(int userId, int ownerId) {
        try {
            sql = "SELECT * FROM `obavestenja` WHERE idVlasnika=" + ownerId + " and idKorisnika=" + userId;
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first()) {
                return false;
            }
            return true;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteLikeFromUser(int userId)
    {
        try{
            sql = "DELETE FROM lajkovi where idKorisnika="+userId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteViewingFromUser(int userId)
    {
        try{
            sql = "DELETE FROM razgledanjestana where idKorisnika="+userId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteNotificationFromUser(int userId)
    {
        try{
            sql = "DELETE FROM obavestenja where idKorisnika="+userId+ " or idVlasnika="+userId ;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteLikesFromAdvert(int advertId)
    {
        try{
            sql = "DELETE FROM lajkovi where idOglasa="+advertId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteViewingFromAdvert(int advertId)
    {
        try{
            sql = "DELETE FROM razgledanjestana where idOglasa="+advertId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deletePicturesOfAdvert(int advertId)
    {
        try{
            sql = "DELETE FROM slikeoglasa where idOglasa="+advertId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void deleteHeatAdvert(int advertId)
    {
        try{
            sql = "DELETE FROM oglasgrejanje where idOglasa="+advertId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteNotificationFromAdvert(int advertId)
    {
        try{
            sql = "DELETE FROM obavestenja where idOglasa="+advertId;
            statement.execute(sql);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getOwnerId(int advertId)
    {
        try {
            sql = "SELECT idIzdavaca FROM oglas where id=" + advertId;
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return -1;
            return resultSet.getInt("idIzdavaca");
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public List<Advert> getAdvertsFromDBFromUser(int userId) {
        sql = "SELECT * FROM oglas WHERE idIzdavaca="+userId+"";
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
                return null;

            ArrayList<String> naziviSlikaOglasa = getPicturesFromDB(resultSet.getInt("id"));

            Advert advert = new Advert(resultSet.getInt("id"), resultSet.getString("naziv"), resultSet.getString("tipOglasa"), resultSet.getString("tipObjekta"), resultSet.getString("grad"), resultSet.getString("deoGrada"), resultSet.getString("ulica"), resultSet.getDouble("brojSoba"), resultSet.getDouble("kvadratura"), resultSet.getString("opis"), getGrejanjaZaOglas(resultSet.getInt("id")), resultSet.getDouble("cena"), resultSet.getInt("idIzdavaca"),naziviSlikaOglasa);
            ArrayList<Advert> adverts = new ArrayList<Advert>();
            adverts.add(advert);

            while (resultSet.next()) {
                naziviSlikaOglasa = getPicturesFromDB(resultSet.getInt("id"));

                advert = new Advert(resultSet.getInt("id"), resultSet.getString("naziv"), resultSet.getString("tipOglasa"), resultSet.getString("tipObjekta"), resultSet.getString("grad"), resultSet.getString("deoGrada"), resultSet.getString("ulica"), resultSet.getDouble("brojSoba"), resultSet.getDouble("kvadratura"), resultSet.getString("opis"), getGrejanjaZaOglas(resultSet.getInt("id")), resultSet.getDouble("cena"), resultSet.getInt("idIzdavaca"),naziviSlikaOglasa);
                adverts.add(advert);
            }
            return adverts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateUser(User user){
        sql = "UPDATE korisnik SET ime='"+user.getIme()+"', prezime='"+user.getPrezime()+"', brTelefona='"+user.getBrTelefona()+"', email='"+user.getMail()+"', sifra=AES_ENCRYPT('"+user.getSifra()+"', '2&!4mvUY#LiA973M7JXA') WHERE id="+user.getId();
        try {
            statement.execute(sql);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public String like(int userId, int advertId)
    {
        sql = "SELECT * FROM Lajkovi where idKorisnika="+userId+ " and idOglasa="+advertId;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first()){
                sql = "INSERT INTO `Lajkovi` (`idOglasa`, `idKorisnika`) VALUES ("+advertId+","+userId+")";
                statement.execute(sql);
                return "like";
            }
            sql = "DELETE FROM Lajkovi WHERE idKorisnika="+userId+" and idOglasa="+advertId;
            statement.execute(sql);
            return "unlike";
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public String isLiked(int userId, int advertId)
    {
        sql = "SELECT * FROM Lajkovi where idKorisnika="+userId+ " and idOglasa="+advertId;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
            {
                return "unlike";
            }
            return "like";
        }
        catch (SQLException e) {
        throw new RuntimeException(e);
        }
    }

    public int numberOfLikesAdvert(int advertId)
    {
        sql = "SELECT count(id) as broj FROM lajkovi where idOglasa="+advertId;
        try {
            ResultSet resultSet = statement.executeQuery(sql);
            if (!resultSet.first())
            {
                return 0;
            }
            return resultSet.getInt("broj");
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }


}

