package com.example.demo.users;

public class User
{
    private int id;
    private String ime;
    private String prezime;
    private String brTelefona;
    private String mail;
    private String sifra;
    private int brojOglasa;
    private double prosecnaOcena;
    private String slika;

    public User()
    {

    }

    public User(String ime, String prezime, String slika, String brTelefona, String mail, String sifra, int brojOglasa, double prosecnaOcena) {
        this.ime = ime;
        this.prezime = prezime;
        this.sifra = sifra;
        this.mail = mail;
        this.brTelefona = brTelefona;
        this.brojOglasa = brojOglasa;
        this.prosecnaOcena = prosecnaOcena;
        this.slika = slika;
    }

    public User(String mail)
    {
        this.mail = mail;
    }


    public User(int id, String ime, String prezime, String slika, String brTelefona, String mail, String sifra, int brojOglasa, double prosecnaOcena) {
        this(ime, prezime, slika, brTelefona, mail, sifra, brojOglasa, prosecnaOcena);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getSifra() {
        return sifra;
    }

    public String getMail()
    {
        return mail;
    }
    public String getBrTelefona()
    {
        return brTelefona;
    }

    public int getBrojOglasa()
    {
        return brojOglasa;
    }

    public double getProsecnaOcena()
    {
        return prosecnaOcena;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setBrTelefona(String brTelefona) {
        this.brTelefona = brTelefona;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }

    public void setBrojOglasa(int brojOglasa) {
        this.brojOglasa = brojOglasa;
    }

    public void setProsecnaOcena(double prosecnaOcena) {
        this.prosecnaOcena = prosecnaOcena;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }
}
