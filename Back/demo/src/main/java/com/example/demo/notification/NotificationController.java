package com.example.demo.notification;

import com.example.demo.db.DB;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.demo.users.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/notifications")
public class NotificationController {

    @PostMapping(value = "/all", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Notification> getAllNotificationForOwnerByID(@RequestBody User user)
    {
        DB db = new DB();
        System.out.println("email"+user.getMail());
        int ownerId = db.getIdUserFromDB(user.getMail());
        System.out.println("Ownerid"+ownerId);
        return db.getAllNotificationForOwner(ownerId);
    }
    @PostMapping(value = "/all2", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public List<Notification> getAllNotificationForUserByID(@RequestBody User user)
    {
        DB db = new DB();
        System.out.println("email"+user.getMail());
        int userId = db.getIdUserFromDB(user.getMail());
        System.out.println("Userid"+userId);
        return db.getAllNotificationForUser(userId);
    }

    @PostMapping(value = "/response", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String responseToScheduling(@RequestBody Notification notification)
    {
        DB db = new DB();
        if (notification.getResponse().equals("DA")) {
            db.confirmViewing(notification.getId(), notification.getAdverId(), notification.getUserId());
            return "zakazano";
        }
        db.cancleViewing(notification.getId(), notification.getAdverId(), notification.getUserId());
        return "otkazano";
    }

    @PostMapping(value = "/delete", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public String deleteNotification(@RequestBody Notification notification)
    {
        DB db = new DB();
        db.deleteTheNotification(notification.getId());
        return "obrisan je oglas";

    }
}
