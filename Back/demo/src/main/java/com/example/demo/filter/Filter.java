package com.example.demo.filter;

import java.util.ArrayList;

public class Filter
{
    private String grad;//+
    private ArrayList<String> grejanje;
    private int maxCena;//+
    private int maxKvadratura;//+
    private int maxSoba;//+
    private int minCena;//+
    private int minKvadratura;//+
    private double minSoba;//+
    private String okvirnaLokacija;//+
    private String tipOglasa;//+

    private ArrayList<String> tipObjekta;

    private int idFiltriranogOglasa;
    private ArrayList<Integer> idjevi;

    public Filter(String grad, ArrayList<String> grejanje, int maxCena, int maxKvadratura, int maxSoba, int minCena, int minKvadratura, double minSoba, String okvirnaLokacija, String tipOglasa ,ArrayList<String> tipObjekta) {
        this.grad = grad;
        this.grejanje = grejanje;
        this.maxCena = maxCena;
        this.maxKvadratura = maxKvadratura;
        this.maxSoba = maxSoba;
        this.minCena = minCena;
        this.minKvadratura = minKvadratura;
        this.minSoba = minSoba;
        this.okvirnaLokacija = okvirnaLokacija;
        this.tipOglasa = tipOglasa;
        this.tipObjekta = tipObjekta;
    }

    public Filter(int idFiltriranogOglasa)
    {
        this.idFiltriranogOglasa = idFiltriranogOglasa;
    }

    public Filter()
    {

    }

    public int getIdFiltriranogOglasa() {
        return idFiltriranogOglasa;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public ArrayList<String> getGrejanje() {
        return grejanje;
    }

    public void setGrejanje(ArrayList<String> grejanje) {
        this.grejanje = grejanje;
    }

    public int getMaxCena() {
        return maxCena;
    }

    public void setMaxCena(int maxCena) {
        this.maxCena = maxCena;
    }

    public int getMaxKvadratura() {
        return maxKvadratura;
    }

    public void setMaxKvadratura(int maxKvadratura) {
        this.maxKvadratura = maxKvadratura;
    }

    public int getMaxSoba() {
        return maxSoba;
    }

    public void setMaxSoba(int maxSoba) {
        this.maxSoba = maxSoba;
    }

    public int getMinCena() {
        return minCena;
    }

    public void setMinCena(int minCena) {
        this.minCena = minCena;
    }

    public int getMinKvadratura() {
        return minKvadratura;
    }

    public void setMinKvadratura(int minKvadratura) {
        this.minKvadratura = minKvadratura;
    }

    public double getMinSoba() {
        return minSoba;
    }

    public void setMinSoba(double minSoba) {
        this.minSoba = minSoba;
    }

    public String getOkvirnaLokacija() {
        return okvirnaLokacija;
    }

    public void setOkvirnaLokacija(String okvirnaLokacija) {
        this.okvirnaLokacija = okvirnaLokacija;
    }

    public String getTipOglasa() {
        return tipOglasa;
    }

    public void setTipOglasa(String tipOglasa) {
        this.tipOglasa = tipOglasa;
    }

    public ArrayList<String> getTipObjekta() {
        return tipObjekta;
    }
}
