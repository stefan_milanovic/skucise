//LISTENER ZA BROJ SOBA
document.querySelector("#minS").addEventListener('change',function()
{
    if(this.value=="unesi")
        document.querySelector("#inpBrojSoba").style="width: 70px; display: visible";
    else
        document.querySelector("#inpBrojSoba").style="display: none";

});
// DeloviGrada
document.querySelector("#gradoviIzbor").addEventListener('change',function()
{
    if(this.value.match("novGrad"))
    {
        document.querySelector("#novGrad").style=`display:"visible";`; 
        document.querySelector("#novDeoGrada").style=`display:"visible";`;

        document.querySelector("#deloviiGradaIzbor").disabled="disabled";
    }
    else
    {
        document.querySelector("#novGrad").style.display="none";
        document.querySelector("#novDeoGrada").style.display="none";
        
        document.querySelector("#deloviiGradaIzbor").disabled="";
    }
});

document.querySelector("#deloviiGradaIzbor").addEventListener('change',function()
{
    if(this.value.match("novDeoGrada"))
    {
        document.querySelector("#novDeoGrada").style=`display:"visible";`;
    }
    else
    {
        document.querySelector("#novDeoGrada").style.display="none";
    }
});
var photos=null;
var idOglasa=null;
// Dodavanje slika
document.querySelector("#files").addEventListener("change", function (e) 
{ //CHANGE EVENT FOR UPLOADING PHOTOS
  if (window.File && window.FileReader && window.FileList && window.Blob) 
  { //CHECK IF FILE API IS SUPPORTED

    const files = e.target.files; //FILE LIST OBJECT CONTAINING UPLOADED FILES
    photos=files;
    const output = document.querySelector("#result");
    output.innerHTML = "";

    for (let i = 0; i < files.length; i++) 
    { // LOOP THROUGH THE FILE LIST OBJECT
        const picReader = new FileReader(); // RETRIEVE DATA URI 

        picReader.addEventListener("load", function (event) 
	        { // LOAD EVENT FOR DISPLAYING PHOTOS
	          const picFile = event.target;
	          const div = document.createElement("div");
              div.classList+="col-sm";
	          div.innerHTML = `<img class="thumbnail" src="${picFile.result}"/>`;
	          output.appendChild(div);
	        });

        picReader.readAsDataURL(files[i]); //READ THE IMAGE
    }
  } else {
    alert("Your browser does not support File API");
  }
});
// console.log(window.src);
window.addEventListener('load', function () {
	files=document.querySelector("#files");
	const output = document.querySelector("#result");
	if(files.files.length > 0)
	{
		files=files.files;
		for (let i = 0; i < files.length; i++) 
		{
			const picReader = new FileReader();
			
			picReader.addEventListener("load", function (event) 
	        { // LOAD EVENT FOR DISPLAYING PHOTOS
                const picFile = event.target;
                const div = document.createElement("div");
                div.classList+="col-sm";
                div.innerHTML = `<img class="thumbnail" src="${picFile.result}"/>`;
                output.appendChild(div);
	        });

			picReader.readAsDataURL(files[i]);
			// console.log(files[i]);	
		}
		
	}
  });


//PRIKUPLJANJE PODATAKA
let tipObjekta=null;
let pre;
let arrayButtonsString=["kuca","stan","poslovni_prostor","vikendica"]
function setTipObjekta(ind,img)
{
    if(tipObjekta==null)
    {
        tipObjekta = arrayButtonsString[ind];
        img.src=`assets/img/icons/${img.id}ACTIVE.png`;
        pre=img;
    }
    else
    {
        pre.src=`assets/img/icons/${pre.id}.png`;
        tipObjekta = arrayButtonsString[ind];
        img.src=`assets/img/icons/${img.id}ACTIVE.png`;
        pre=img;
    }
}
function dajGrad()
{
    g = document.querySelector("#gradoviIzbor").value;
    if(g.length == 0)
    {
        document.querySelector("#greskaGrad").innerHTML="Morate izabrati grad";
        return false;
    }
    else if(g == "novGrad")
    {
        g = document.querySelector("#novGrad").value;
        r = new RegExp('^[A-Z][a-z \s A-Z]+$');
        if(g.match(r))
            return g;
        document.querySelector("#greskaGrad").innerHTML="Grad mora poceti velikim slovom";
        return false;
    }
    else
        return g;
}
function dajDeoGrada()
{
    A = document.querySelector("#gradoviIzbor").value
    g = document.querySelector("#deloviiGradaIzbor").value;
    if(g.length == 0)
    {
        document.querySelector("#greskaDeoGrada").innerHTML="Morate izabrati deo grada";
        return false;
    }
    else if(A == "novGrad")
    {
        g = document.querySelector("#novDeoGrada").value;
        r = new RegExp('^[A-Z][a-z \s A-Z]+$');
        console.log(g);
        if(g.match(r))
        {
            return g;
        } 
        document.querySelector("#greskaDeoGrada").innerHTML="Mora poceti velikim slovom";
        return false;
    }
    else if(g == "novDeoGrada")
    {
        g = document.querySelector("#novDeoGrada").value;
        r = new RegExp('^[A-Z][a-z \s A-Z]+$');
        console.log(g);
        if(g.match(r))
        {
            return g;
        } 
        document.querySelector("#greskaDeoGrada").innerHTML="Mora poceti velikim slovom";
        return false;
    }
    else if(g == "-")
    {
        document.querySelector("#greskaDeoGrada").innerHTML="Morate izabrati deo grada";
        return false;
    }
    else
        return g;
}
function dajKvadraturu()
{
    inpKvadratura=document.querySelector("#inpKvadratura").value;
    inpKvadratura=parseFloat(inpKvadratura);
    if(isNaN(inpKvadratura))
    {
        document.querySelector("#greskaKvadratura").innerHTML="*Popunite ovo polje";
        return false
    }
    return parseFloat(inpKvadratura);    
}
function dajCenu()
{
    inpCena=document.querySelector("#inpCena").value;
    inpCena=parseFloat(inpCena);
    if(isNaN(inpCena))
    {
        document.querySelector("#greskaCena").innerHTML="*Popunite ovo polje";
        return false
    }
    return parseFloat(inpCena);    
}
function dajGrejanje()
{
    grejanje=new Array();
    selektovano=document.getElementsByClassName("c1");
	for (e of selektovano)
	{
		if(e.checked)
		{
			grejanje.push(e.value);
		}
	}
    return grejanje;
}
function dajBrojSoba()
{
    if(document.querySelector("#minS").value == "unesi")
    {
        s = document.querySelector("#inpBrojSoba").value;
        s=parseFloat(s);
        if(isNaN(s))
        {
            document.querySelector("#greskaBrojSoba").innerHTML="*Popunite ovo polje";
            return false
        }
        console.log(s);
        return parseFloat(s);
    }
    else
    {
        s = document.querySelector("#minS").value;
        s=parseFloat(s);
        if(isNaN(s))
        {
            document.querySelector("#greskaBrojSoba").innerHTML="*Popunite ovo polje";
            return false
        }
        return parseFloat(s);
    }

}

// TIP OGLASA
indikator=0;			//koji je aktivan

function dajTipOglasa(ind)
{
	ind = parseInt(ind);
	if(ind > 0 && indikator != 1)
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: #ffb727;";
		a=document.getElementById("aProdaja");
		a.style="color: ;";
		indikator = 1;
	}
	else if(ind < 0 && indikator != -1)
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: ;";
		a=document.getElementById("aProdaja");
		a.style="color: #ffb727;";
		indikator = -1;
	}
	else
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: ;";
		a=document.getElementById("aProdaja");
		a.style="color: ;";		
		indikator = 0;
	}
}
function stringTipOglasa()
{
	//[izdavanje / prodaja]
	if (indikator > 0)
		return "izdavanje";
	else if (indikator < 0)
		return "prodaja";
    else
        return false;
}

function dajNazivOglasa()
{
    let nazivOglasa = document.querySelector("#nazivOglasa").value;
    if(nazivOglasa.length>0)
        return nazivOglasa;
    document.querySelector("#greskaNaziv").innerHTML="Popunite ovo polje";
    return null;
}

// DUGME PROVERA + KREIRANJE OOBJEKTA I SLANJE
function buttonGet()
{
    osvezi();
    let tipOglasa = stringTipOglasa(); 
    // izostavljeno uzimanje vrednosti
    //let nazivOglasa = document.querySelector("#nazivOglasa"); 
    let nazivOglasa = dajNazivOglasa();
    let grad=dajGrad();
    let deoGrada = dajDeoGrada();
    let brojSoba = dajBrojSoba();
    let kvadratura = dajKvadraturu();
    let ulica = document.querySelector("#ulica").value;
    let grejanje = dajGrejanje();
    let cena = dajCenu();
    let slike = document.querySelector("#files");
    let opis = document.querySelector("#description").value;
    console.log(slike);
    console.log(slike.files);
    //console.log(kvadratura);

    // nazivOglasa.value.length je bilo
    if(tipOglasa && nazivOglasa!=null && tipObjekta!=null && grad && deoGrada && brojSoba && kvadratura && cena && grejanje.length>0 && slike.files.length > 0)
    {
        let kreiraj = new Object();
        //promenjeno iz 'nazivOglasa' u 'naziv' kako bi se poklopilo sa nazivima na back-u
        //kreiraj.nazivOglasa = nazivOglasa; 
        kreiraj.naziv = nazivOglasa;  //String
        kreiraj.tipOglasa = tipOglasa;      //String
        kreiraj.tipObjekta = tipObjekta;    //String
        kreiraj.grad = grad;                //String
        kreiraj.deoGrada = deoGrada;        //String
        kreiraj.ulica = ulica;              //String
        kreiraj.brojSoba = brojSoba;       //Float 
        kreiraj.kvadratura = kvadratura;   //Float
        kreiraj.opis = opis;               //String
        kreiraj.cena = cena;               //Float
        kreiraj.idIzdavaca=sessionStorage.idKorisnika;

        // promenjen naziv iz 'grejanje' u 'naziviGrejanja' kako bi se poklopilo sa nazivima na back-u
        //kreiraj.grejanje = grejanje;
        kreiraj.naziviGrejanja = grejanje;        //Array string
    
        console.log(kreiraj);

        var xmlhttp = new XMLHttpRequest(); 
        var theUrl = "http://localhost:8081/adverts/m10";
	    xmlhttp.open("POST", theUrl,true);

        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
            {
                console.log(xmlhttp.response);
                if(xmlhttp.response==-1)
                    alert("Neuspesno upisivanje oglasa u bazu!");
                // parsiranje u json je smetalo, nije potrebno
			    //var json=JSON.parse(xmlhttp.responseText);
			    //console.log(json);
                // ovako može
                else
                {
                    idOglasa=xmlhttp.responseText;
                    posaljiSlike(slike.files, xmlhttp.responseText);
                    
                    
                }
                


                console.log(xmlhttp.responseText);
            }
        }
        console.log("salje");
        xmlhttp.send(JSON.stringify(kreiraj));
    }
    else
    {
        if(! nazivOglasa.length > 0)
            nazivOglasa.style="border-color:red;";
        if(! tipOglasa)
            document.querySelector("#greskaTipOglasa").innerHTML="*Popunite ovo polje";
        if(tipObjekta==null)
            document.querySelector("#greskaTipObjekta").innerHTML="*Popunite ovo polje";
        if(! grejanje.length > 0)
            document.querySelector("#greskaGrejanje").innerHTML="*Popunite ovo polje";
        if(! slike.files.length > 0)
        {
            document.querySelector("#greskaSlike").innerHTML="*Unesti slike";
        }
    }

}
function proba()
{
    document.querySelector("#alert").style=`display:"visible";`; 
    document.getElementById("filter").reset();
    document.getElementById("result").innerHTML="";

}
function posaljiSlike(fajlovi,id)
{
    for(let i=0;i<fajlovi.length;i++)
    {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        var theUrl = "http://localhost:8081/adverts/upload";
        xmlhttp.open("POST", theUrl,true);
        console.log(fajlovi);
        console.log(id);
        let fd = new FormData;
        fd.append("files",fajlovi[i]);
        fd.append("idOglasa",id);
        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if(xmlhttp.responseText=="uspesno")
                {
                    document.querySelector("#alert").style=`display:"visible";`; 
                    document.getElementById("filter").reset();
                    document.getElementById("result").innerHTML="";
                }
            }
        }
        xmlhttp.send(fd);
    }
}
function osvezi()
{
    document.querySelector("#nazivOglasa").style=`border-color:"";`;
    document.querySelector("#greskaTipOglasa").innerHTML="";
    document.querySelector("#greskaTipObjekta").innerHTML="";
    document.querySelector("#greskaGrad").innerHTML="";
    document.querySelector("#greskaDeoGrada").innerHTML="";
    document.querySelector("#greskaBrojSoba").innerHTML="";
    document.querySelector("#greskaKvadratura").innerHTML="";
    document.querySelector("#greskaCena").innerHTML="";
    document.querySelector("#greskaGrejanje").innerHTML="";
    document.querySelector("#greskaSlike").innerHTML="";
    
}