/*--------------------------------------------------------------
# Filter value and effects
--------------------------------------------------------------*/
var primljeniOglasi;
var filtrirani=null;
var dodat=document.querySelector("#dodavanje");
arrayButtons=[-1,-1,-1,-1];	//koji je aktivan button
arrayButtonsString=["kuca","stan","poslovni_prostor","vikendica"]
function setArrayAndColor(ind,img)
{
	ind=parseInt(ind);
	arrayButtons[ind]*=-1
	if(arrayButtons[ind]>0)
	{
		img.src=`assets/img/icons/${img.id}ACTIVE.png`;
		document.querySelector(`#${img.id}A`).style=`color:#ffb727;`;
	}
	else
	{
		img.src=`assets/img/icons/${img.id}.png`;
		document.querySelector(`#${img.id}A`).style=`color:"";`;
	}

}
function listActiveButtons()
{
	//[kuca,stan,poslovni_prostor,vikendica]
	let activeButtons=new Array();
	for(el in arrayButtons)
	{
		if(arrayButtons[el]==1)
		{
			activeButtons.push(arrayButtonsString[el]);
		}
	}
	if(activeButtons.length>0)
		return activeButtons;
}
/*--------------------------------------------------------------
# Izdavanje / prodaja
--------------------------------------------------------------*/
indikator=0;			//koji je aktivan
arrayTipOglasaString=["izdavanje","prodaja"];

function setTipOglasa(ind)
{
	ind = parseInt(ind);
	if(ind > 0 && indikator != 1)
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: #ffb727;";
		a=document.getElementById("aProdaja");
		a.style="color: ;";
		indikator = 1;
	}
	else if(ind < 0 && indikator != -1)
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: ;";
		a=document.getElementById("aProdaja");
		a.style="color: #ffb727;";
		indikator = -1;
	}
	else
	{
		a=document.getElementById("aIzdavanje");
		a.style="color: ;";
		a=document.getElementById("aProdaja");
		a.style="color: ;";		
		indikator = 0;
	}
}

function stringTipOglasa()
{
	//[izdavanje / prodaja]
	if (indikator > 0)
		return "izdavanje";
	else if (indikator < 0)
		return "prodaja";
}

/*--------------------------------------------------------------
# Search button
--------------------------------------------------------------*/
function searchButtonGet()
{
	// formCity, minQ, maxQ, minM, maxM, partOfCity, minS,maxS
	inputs=document.getElementById("filter").elements;
	inputs2=document.getElementsByClassName("c1");

	// console.log(inputs["formCity"].value);
	// console.log(inputs["minQ"].value);
	// for(e of inputs2)
	// {
	// 	if(e.checked)
	// 	{
	// 		console.log(e.value);
	// 	}
	// }

	var pretrazi = new Object();
//tipOglasa 
	tipoglasa=stringTipOglasa();
	if(typeof tipoglasa !== "undefined")
		pretrazi.tipOglasa=tipoglasa;
//tipObjekta 
	activeButtons=listActiveButtons();
	if(typeof activeButtons !== "undefined")
		pretrazi.tipObjekta=activeButtons;
//grad 
	grad=inputs["formCity"].value;
	if(grad != "-")
		pretrazi.grad = grad;
//okvirnaLokacija 
	okvirnaL=inputs["partOfCity"].value;
	if(okvirnaL != "-")
		pretrazi.deoGrada=okvirnaL;
//brojSoba 
	minS=parseFloat(inputs["minS"].value);
	maxS=parseFloat(inputs["maxS"].value);
	if(!isNaN(minS))
		pretrazi.minSoba = minS;
	if(!isNaN(maxS))
		pretrazi.maxSoba = maxS;
	if(inputs["minS"].value=="5+")
		pretrazi.minSoba = 6;
	if(inputs["maxS"].value=="5+")
		pretrazi.maxSoba = 6;
//kvadratura  JEDAN NACIN SLANJA
	minQ=parseFloat(inputs["minQ"].value);
	maxQ=parseFloat(inputs["maxQ"].value);
	if(!isNaN(minQ))
		pretrazi.minKvadratura = minQ;
	if(!isNaN(maxQ))
		pretrazi.maxKvadratura = maxQ;

//grejanje
	// naziv=
	grejanje=new Array();
	for (e of inputs2)
	{
		if(e.checked)
		{
			grejanje.push(e.value);
		}
	}
//	!!!!!! ovo proveri
	if(grejanje.length > 0)
		pretrazi.grejanje = grejanje;
//cena
	minM=parseFloat(inputs["minM"].value);
	maxM=parseFloat(inputs["maxM"].value);
	if(!isNaN(minM))
		pretrazi.minCena = minM;
	if(!isNaN(maxM))
		pretrazi.maxCena = maxM;


	console.log(pretrazi);


	var xmlhttp = new XMLHttpRequest(); 
    var theUrl = "http://localhost:8081/filter/f1";
	xmlhttp.open("POST", theUrl,true);

    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {

			//console.log(xmlhttp.responseText);
			filtrirani=xmlhttp.responseText;
			if(filtrirani!=null)
				ispisiFiltrirane(filtrirani);
			else
				dodat.innerHTML="";
			//primljeniOglasi=json;
			//console.log(json[0].grad);
		

			// var dodat=document.querySelector("#dodavanje");
			// dodat.innerHTML="";
			// ispisiOglase(json);
        }
    }

    xmlhttp.send(JSON.stringify(pretrazi));
}
function sviOglasi(){
    let httpRequest = new XMLHttpRequest();
    httpRequest.open("GET","http://localhost:8081/adverts/m1");

    
    httpRequest.send();
    httpRequest.onload=function(){
        // console.log(httpRequest.responseText);
		var json=JSON.parse(httpRequest.responseText);
		primljeniOglasi=json;
		//console.log(json[0].grad);
		ispisiOglase(json);
		// console.log(httpRequest.responseText);
    }

}

function ispisiOglase(json)
{
	dodat.innerHTML="";
	for(let i=0; i<json.length;i++)
	{
		if(json[i].naziviSlikaOglasa==null)
			dodat.innerHTML+='<div onclick="otvoriOglas('+json[i].advertId+')" class="col-lg d-flex justify-content-center d-flex flex-column"><div class="d-flex justify-content-center divImg"><img src="assets/img/slikeOglasa/NoImageFound.jpg.png" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+json[i].grad+'</span> </p><p>Lokacija: <span>'+json[i].deoGrada+'</span> </p><p>Kvadratura: <span>'+json[i].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+json[i].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+json[i].cena+' €</span></p></div></div>';
		else
			dodat.innerHTML+='<div onclick="otvoriOglas('+json[i].advertId+')" class="col-lg d-flex justify-content-center d-flex flex-column "><div class="d-flex justify-content-center divImg"><img src="assets/img/slikeOglasa/'+json[i].advertId+'/'+json[i].naziviSlikaOglasa[0]+'" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+json[i].grad+'</span> </p><p>Lokacija: <span>'+json[i].deoGrada+'</span> </p><p>Kvadratura: <span>'+json[i].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+json[i].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+json[i].cena+' €</span></p></div></div>';
		
		//console.log(i);
		//dodat.innerHTML+='<div onclick="otvoriOglas('+json[i].advertId+')" class="col-lg"><div class="d-flex justify-content-center"><img src="assets/img/stan.jpg" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+json[i].grad+'</span> </p><p>Lokacija: <span>'+json[i].deoGrada+'</span> </p><p>Kvadratura: <span>'+json[i].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+json[i].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+json[i].cena+' €</span></p></div></div>';
		// console.log(json[i].advertId);
	}
}
function ispisiFiltrirane(filtrirani)
{
	console.log(filtrirani);
	if(filtrirani.length==0)
	{
		// dodat.innerHTML="Nema";
		str=`<div style="text-align:center;">Oglas tog tipa ne postoji.</div>`
		dodat.innerHTML=str;
	}
	else{
		filtrirani = JSON.parse(filtrirani);
		console.log("aaa"+filtrirani);
		dodat.innerHTML="";
		for(let i=0;i<filtrirani.length;i++)
		{
			for(let j=0;j<primljeniOglasi.length;j++)
			{
				//console.log(filtrirani[i]);
				if(filtrirani[i].idFiltriranogOglasa==primljeniOglasi[j].advertId)
				{
					if(primljeniOglasi[j].naziviSlikaOglasa==null)
						dodat.innerHTML+='<div onclick="otvoriOglas('+primljeniOglasi[j].advertId+')" class="col-lg d-flex justify-content-center d-flex flex-column"><div class="d-flex justify-content-center divImg"><img src="assets/img/slikeOglasa/NoImageFound.jpg.png" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+primljeniOglasi[j].grad+'</span> </p><p>Lokacija: <span>'+primljeniOglasi[j].deoGrada+'</span> </p><p>Kvadratura: <span>'+primljeniOglasi[j].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+primljeniOglasi[j].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+primljeniOglasi[j].cena+' €</span></p></div></div>';
					else
						dodat.innerHTML+='<div onclick="otvoriOglas('+primljeniOglasi[j].advertId+')" class="col-lg d-flex justify-content-center d-flex flex-column"><div class="d-flex justify-content-center divImg"><img src="assets/img/slikeOglasa/'+primljeniOglasi[j].advertId+'/'+primljeniOglasi[j].naziviSlikaOglasa[0]+'" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+primljeniOglasi[j].grad+'</span> </p><p>Lokacija: <span>'+primljeniOglasi[j].deoGrada+'</span> </p><p>Kvadratura: <span>'+primljeniOglasi[j].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+primljeniOglasi[j].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+primljeniOglasi[j].cena+' €</span></p></div></div>';
			
				}
			}
		}
	}

}
function isprobaj(){
	var data='[{"naziv":"Stan za studente","tipOglasa":"izdavanje","tipObjekta":"stan","grad":"Jagodina","deoGrada":"Blizu opštine","ulica":"Knjeginje Milice","brojSoba":2,"kvadratura":56.0,"opis":null,"idGrejanja":2,"advertId":2,"cena":300.0,"idIzdavaca":3,"naziviSlikaOglasa":[null]},{"naziv":"Kuća na prodaju","tipOglasa":"prodaja","tipObjekta":"kuća","grad":"Jagodina","deoGrada":"Kod restorana fontana","ulica":"Stevana Jakovljevića","brojSoba":8,"kvadratura":200.0,"opis":"Mnogo je lepa kuća","idGrejanja":1,"advertId":1,"cena":1000000.0,"idIzdavaca":2,"naziviSlikaOglasa":[null]},{"advertId":69,"naziv":"Naziv oglasa123","tipOglasa":"izdavanje","tipObjekta":"kuca","grad":"Jagodina","deoGrada":"Blizu opštine","ulica":"adresa324","brojSoba":1,"kvadratura":1.0,"opis":"Opis123","naziviGrejanja":["Gas","Podno"],"cena":1.0,"idIzdavaca":18,"naziviSlikaOglasa":["monalisa-v.jpg","website.jpg"]},{"advertId":69,"naziv":"Naziv oglasa123","tipOglasa":"izdavanje","tipObjekta":"kuca","grad":"Jagodina","deoGrada":"Blizu opštine","ulica":"adresa324","brojSoba":1,"kvadratura":1.0,"opis":"Opis123","naziviGrejanja":["Gas","Podno"],"cena":1.0,"idIzdavaca":18,"naziviSlikaOglasa":["monalisa-v.jpg","website.jpg"]}]';
	console.log(data);
	var json=JSON.parse(data);
	primljeniOglasi=json;
	console.log(json[0].grad);
	var dodat=document.querySelector("#dodavanje");
	for(let i=0; i<json.length;i++)
	{
		if(json[i].naziviSlikaOglasa[0]==null)
			dodat.innerHTML+='<div onclick="otvoriOglas('+json[i].advertId+')" class="col-lg"><div class="d-flex justify-content-center"><img class="slika" src="assets/img/slikeOglasa/NoImageFound.jpg.png" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+json[i].grad+'</span> </p><p>Lokacija: <span>'+json[i].deoGrada+'</span> </p><p>Kvadratura: <span>'+json[i].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+json[i].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+json[i].cena+' €</span></p></div></div>';
		else
			dodat.innerHTML+='<div onclick="otvoriOglas('+json[i].advertId+')" class="col-lg"><div class="d-flex justify-content-center"><img class="slika" src="assets/img/slikeOglasa/'+json[i].advertId+'/'+json[i].naziviSlikaOglasa[0]+'" class="imgElement"></div><div class="infoElement"><p>Grad: <span>'+json[i].grad+'</span> </p><p>Lokacija: <span>'+json[i].deoGrada+'</span> </p><p>Kvadratura: <span>'+json[i].kvadratura+'m<sup>2</sup></span> </p><p>Broj soba: <span>'+json[i].brojSoba+'</span> </p><p style="text-align: center;">Cena: <span>'+json[i].cena+' €</span></p></div></div>';
	}
	//dodat.innerHTML='<div class="col-lg"><div class="d-flex justify-content-center"><img src="assets/img/stan.jpg" class="imgElement"></div><div class="infoElement"><p>Grad: <span>asd</span> </p><p>Lokacija: <span>asd</span> </p><p>Kvadratura: <span>asdm<sup>2</sup></span> </p><p>Broj soba: <span>asc</span> </p><p style="text-align: center;">Cena: <span>asd €</span></p></div></div>';
	
	console.log(dodat);
}
var oglas;
function otvoriOglas(i)
{
	console.log(i);
	//console.log(primljeniOglasi[i]);
	
	for(let j=0;j<primljeniOglasi.length;j++)
		if(primljeniOglasi[j].advertId==i)
			oglas=primljeniOglasi[j];
	// console.log(oglas);
	sessionStorage.removeItem("oglas");
	sessionStorage.setItem("oglas",JSON.stringify(oglas));
	window.open("./oglas.html");
}


