const em=document.querySelector("#email");
const loz=document.querySelector("#lozinka");

const emailHelp2 = document.querySelector('#emailHelp2');
const lozinkaHelp2 = document.querySelector('#lozinkaHelp2');

em.addEventListener("keyup", function(){
    if(em.checkValidity())
        emailHelp2.style.display="none";
    else
        emailHelp2.style.display="block";
    
})

loz.addEventListener("keyup", function(){
    if(loz.checkValidity())
        lozinkaHelp2.style.display="none";
    else
        lozinkaHelp2.style.display="block";
    
})

function ulogujSe()
{
    const email=document.getElementById("email").value;
    const lozinka=document.getElementById("lozinka").value;
    if(em.checkValidity() && loz.checkValidity())
    {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        var theUrl = "http://localhost:8081/api/m3";
        xmlhttp.open("POST", theUrl,true);

        var params=new Object();

        params.mail=email;
        params.sifra=lozinka;
        console.log(JSON.stringify(params));
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if(xmlhttp.responseText==0)
                {
                    lozinkaHelp2.innerHTML="Pogresna lozinka!";
                    lozinkaHelp2.style.display="block";
                }
                else if(xmlhttp.responseText==-2)
                {
                    alert("Nije registrovan!");
                }
                else if(xmlhttp.responseText==-1)
                {
                    sessionStorage.removeItem("admin");
                    sessionStorage.setItem("admin",email);
                    window.location="./index.html";
                }
                else
                {
                    sessionStorage.removeItem("idKorisnika");
                    sessionStorage.setItem("idKorisnika",xmlhttp.responseText);
                    sessionStorage.removeItem("mail");
                    sessionStorage.setItem("mail",email);
                    // window.open("./index.html");
                    // window.close();
                    window.location="./index.html";
                }
               
            }
        }

        xmlhttp.send(JSON.stringify(params));
    }
    

}

function proba()
{

    sessionStorage.removeItem("admin");
    sessionStorage.setItem("admin","asdaads");
    window.location="./index.html";
    
}