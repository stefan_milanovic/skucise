// Vreme
if(!sessionStorage.mail && !sessionStorage.admin)
    document.querySelector("#zakaziBut").style="display:none;";
function setujBrojSati()
{
    s = document.querySelector("#brojSati");
    str="";
    for(let i=0;i<24;i++)
    {
        if(i < 10)
            str+=`<option>0${i}</option>`;
        else
        {
            if(i == 12)
            str+=`<option selected>${i}</option>`;    
            str+=`<option>${i}</option>`;
        }
    }
    s.innerHTML = str;
}

function setujBrojMinuti()
{
    s = document.querySelector("#brojMinuti");
    str=`<option>00</option>`;
    for(let i=10;i<60;i=i+10)
    {
        str+=`<option>${i}</option>`;    
    }
    s.innerHTML = str;
}

var naziv=document.querySelector("#naziv");

var grad=document.querySelector("#grad");
var ulica=document.querySelector("#ulica");
var tipObjekta=document.querySelector("#tipObjekta");
var kvadratura=document.querySelector("#kvadratura");
var deoGrada=document.querySelector("#deoGrada");
var brojSoba=document.querySelector("#brojSoba");
var grejanje=document.querySelector("#grejanje");
var cena=document.querySelector("#cena");
var slike=document.querySelector("#slike");
var tipOglasa = document.querySelector("#tipOglasa");
var primljen=null;
function oglas()
{
    var email = sessionStorage.getItem("mail");
    var admin = sessionStorage.getItem("admin");
    if(email==null && admin==null)
    document.querySelector("#lajk").style.enabled="false";
    var primljenOglas1=sessionStorage.getItem("oglas");
    var primljenOglas=JSON.parse(primljenOglas1);
    console.log(primljenOglas);
    primljen=primljenOglas;
    daLiJeLajkovan();
    brojLajkova();
    // console.log(primljenOglas);
    if(primljenOglas.naziviSlikaOglasa==null)
        slike.innerHTML+='<div class="swiper-slide"><img src="assets/img/slikeOglasa/NoImageFound.jpg.png" alt=""></div>';
    else
    {
        for(let i=0;i<primljenOglas.naziviSlikaOglasa.length;i++)
        {
            slike.innerHTML+='<div class="swiper-slide"><img src="assets/img/slikeOglasa/'+primljenOglas.advertId+'/'+primljenOglas.naziviSlikaOglasa[i]+'" alt=""></div>';
        }
    }
     
    naziv.innerHTML=primljenOglas.naziv;
    grad.innerHTML=primljenOglas.grad;
    ulica.innerHTML=primljenOglas.ulica;
    tipObjekta.innerHTML=primljenOglas.tipObjekta;
    kvadratura.innerHTML=primljenOglas.kvadratura+"&nbsp;<strong>m<sup>2</sup></strong>";
    deoGrada.innerHTML=primljenOglas.deoGrada;
    brojSoba.innerHTML=primljenOglas.brojSoba;
    tipOglasa.innerHTML=primljenOglas.tipOglasa;
    document.querySelector("#detalji").innerHTML=primljenOglas.opis;
    // grejanje.innerHTML=primljenOglas.grejanje;
    cena.innerHTML=primljenOglas.cena+" €";
    str=""
    for(el in primljenOglas.naziviGrejanja)
    {
        g = primljenOglas.naziviGrejanja[el];
        if(el == (primljenOglas.naziviGrejanja.length - 1))
            str+=g;
        else
            str+=g+", ";
    }
    grejanje.innerHTML=str;
}

function dajDatum()
{
    datum=document.querySelector("#datum");
    return datum.value;
}
function dajVreme()
{
    sat=String(document.querySelector("#brojSati").value);
    minuti=String(document.querySelector("#brojMinuti").value);
    vreme=sat+":"+minuti+":"+"00";
    return vreme;
    
}
function minDatum()
{
    datum = new Date();
    dan=datum.getDate();
    mesec=datum.getMonth() + 1;
    godina=datum.getFullYear();
    if(mesec<10 && dan < 10)
        str=`${godina}-0${mesec}-0${dan}`;
    else if(mesec<10 && dan > 10)
        str=`${godina}-0${mesec}-${dan}`;
    else if(mesec>10 && dan < 10)
        str=`${godina}-${mesec}-0${dan}`;
    else 
        str=`${godina}-${mesec}-0${dan}`;
    console.log(str);
    document.querySelector("#datum").min=str;
}
function zakazi()
{
    datum = dajDatum();
    vreme=dajVreme();
    datum_vreme=datum+" "+vreme
    if(datum.length===0)
    {
        document.querySelector("#datum").style="border-color:red;";
    }
    else
    {
        var oglasInfo=sessionStorage.getItem("oglas");
        var oglasInfo=JSON.parse(oglasInfo);

        // console.log(sessionStorage.idKorisnika);//1
        // console.log(oglasInfo.advertId);//2

        salji = new Object();
        salji.userId = sessionStorage.idKorisnika;
        salji.advertId = oglasInfo.advertId;
        salji.time=datum_vreme;

        console.log(salji);
        salji = JSON.stringify(salji);
        console.log(salji);
        // var xhttp = new XMLHttpRequest();
        
        // xhttp.onreadystatechange = function(){
        //     if(this.readyState == 4 && this.status == 200)
        //     {
        //         console.log(xhttp.responseText);
        //     }
        // }
        // xhttp.open("POST",salji,"http://localhost:8081/api/viewing");
        // xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        // xhttp.send(salji);

        var xmlhttp = new XMLHttpRequest(); 
        var theUrl = "http://localhost:8081/api/viewing";
	    xmlhttp.open("POST", theUrl,true);

        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onreadystatechange = function() {
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) 
            {
                console.log(xmlhttp.responseText);
                if(xmlhttp.responseText=="uspesno")
                    document.querySelector("#alert").style=`display:"visible";`; 
            }
        }
        console.log("salje");
        xmlhttp.send(salji);


    }
}

function daLiJeLajkovan()
{
    

    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    var theUrl = "http://localhost:8081/api/isliked";
    xmlhttp.open("POST", theUrl,true);

    var fd=new FormData();
    fd.append("user",sessionStorage.getItem("mail"));
    fd.append("advertId",primljen.advertId);

    xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if(xmlhttp.responseText=="like")
            {
                document.querySelector("#lajk").style.color="blue";
            }
            else
            {
                document.querySelector("#lajk").style.color="grey";
            }
        }
    }
    xmlhttp.send(fd);
}
function brojLajkova()
{
    

    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    var theUrl = "http://localhost:8081/api/numberoflikes";
    xmlhttp.open("POST", theUrl,true);

    var fd=new FormData();
    fd.append("advertId",primljen.advertId);

    xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("brojLajkova").innerHTML=xmlhttp.responseText;
            document.getElementById("brojLajkova").style="font-size:25px;"
        }
    }
    xmlhttp.send(fd);
}
function lajkuj()
{
    

    var xmlhttp = new XMLHttpRequest();    
    var theUrl = "http://localhost:8081/api/likes";
    xmlhttp.open("POST", theUrl,true);

    var fd=new FormData();
    fd.append("user",sessionStorage.getItem("mail"));
    fd.append("advertId",primljen.advertId);

    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            daLiJeLajkovan();
            brojLajkova();
        }
    }
    xmlhttp.send(fd);
}