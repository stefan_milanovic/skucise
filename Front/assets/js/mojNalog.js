zahtevaju = null;
mojiOglasi = null;
mojeInformacije = null;
salji = new Object();
if (sessionStorage.mail)
    salji.mail = sessionStorage.mail;
else
    salji.mail = sessionStorage.admin;
salji = JSON.stringify(salji);

function Load() {
    loadMojeInfo();
    loadMojiOglasi();
    setujPrikaz();
    loadZahtevaju();
    
    ispisOdgovoraNaZahtev();
    ucitavanjeDaLiJePotvrdjenoOdbijeno();
    
}

function loadZahtevaju() {
    var xmlhttp1 = new XMLHttpRequest();
    var theUrl = "http://localhost:8081/notifications/all";
    xmlhttp1.open("POST", theUrl, true);

    xmlhttp1.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp1.onreadystatechange = function() {
        if (xmlhttp1.readyState == 4 && xmlhttp1.status == 200) {
            // console.log(xmlhttp.responseText);
            if (xmlhttp1.responseText.length > 0) {
                parsirano = JSON.parse(xmlhttp1.responseText);
                // console.log(parsirano);
                zahtevaju = parsirano;
                prikazZahteva();
            }
        }
    }
    xmlhttp1.send(salji);
}

function loadMojiOglasi() {
    var xmlhttp2 = new XMLHttpRequest();
    var theUrl = "http://localhost:8081/adverts/myadvert";
    xmlhttp2.open("POST", theUrl, true);

    xmlhttp2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp2.onreadystatechange = function() {
        if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
            //console.log(xmlhttp.responseText);
            if (xmlhttp2.responseText.length > 0) {
                parsiranoMojiOglasi = JSON.parse(xmlhttp2.responseText);
                // console.log(parsiranoMojiOglasi);
                mojiOglasi = parsiranoMojiOglasi;
                // prikazMojihOglasa();

            }
        }
    }
    xmlhttp2.send(salji);
}

function loadMojeInfo() {
    var xmlhttp3 = new XMLHttpRequest();
    var theUrl = "http://localhost:8081/api/myInfo";
    xmlhttp3.open("POST", theUrl, true);

    xmlhttp3.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp3.onreadystatechange = function() {
        if (xmlhttp3.readyState == 4 && xmlhttp3.status == 200) {
            console.log(xmlhttp3.responseText);
            if (xmlhttp3.responseText.length > 0) {
                parsiranoMojeInformacije = JSON.parse(xmlhttp3.responseText);
                console.log(parsiranoMojeInformacije);
                mojeInformacije = parsiranoMojeInformacije;
                dispMojeInformacije();
            }
        }
    }
    console.log(salji);
    xmlhttp3.send(salji);
}
var odgovorNaZahtev=null;
function ucitavanjeDaLiJePotvrdjenoOdbijeno() 
{
    var xmlhttp3 = new XMLHttpRequest();
    var theUrl = "http://localhost:8081/notifications/all2";
    xmlhttp3.open("POST", theUrl, true);

    xmlhttp3.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp3.onreadystatechange = function() {
        if (xmlhttp3.readyState == 4 && xmlhttp3.status == 200) {
            
            // console.log(xmlhttp3.responseText);
            odgovorNaZahtev = JSON.parse(xmlhttp3.responseText);
            ispisOdgovoraNaZahtev();
        }
    }
    console.log(salji);
    xmlhttp3.send(salji);
}

function ispisOdgovoraNaZahtev()
{
    // console.log("Odgovor da li je potvrdjeno:")
    // console.log(odgovorNaZahtev);
    str="";
    for(el in odgovorNaZahtev)
    {
        console.log(odgovorNaZahtev[el]);
        obj = odgovorNaZahtev[el];
        // console.log(obj.naziviSlikaOglasa[0]);
        time=obj.time;
        time=time.split(" ");
        datum=time[0];
        vreme=time[1];
        if(obj.response.match("DA"))
        {
            str=`<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
                <img src="assets/img/slikeOglasa/${obj.id}/${obj.naziviSlikaOglasa[0]}">
            </div>
            <div class="col-sm ">
                <h5>${obj.nazivOglasa}</h5>
                <p>Datum: ${datum}</p>
                <p>Vreme: ${vreme}</p>
                <h5 style="color:#40b30e">Zakazan pregled</h5>
            </div>
            <hr class="mt-4 mb-4">  
          </div>`
        }
        else
        {
            str+=`<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
                <img src="assets/img/stan.jpg">
            </div>
            <div class="col-sm ">
                <h5>${obj.nazivOglasa}</h5>
                <p>Datum: ${datum}</p>
                <p>Vreme: ${vreme}</p>
                <h5 style="color:#40b30e">Zakazan pregled</h5>
            </div>
            <div class="col-sm d-flex align-items-center justify-content-end odgovor">
            </div>
            <hr class="mt-4 mb-4">  
          </div>`
        }
    }
    document.querySelector("#elementi").innerHTML+=str;
}

elementi = document.querySelector("#elementi")


function prikazZahteva() {
    elementi.innerHTML = "";
    for (e in zahtevaju) {
        // console.log(zahtevaju[e]);
        let id;
        obj = zahtevaju[e];
        for(let i=0;i<mojiOglasi.length;i++)
        {
            if(obj.advertId==mojiOglasi[i].advertId)
                id=i;
        }
        vreme = obj.time.split(" ");
        
        if(obj.response==null)
        {
            console.log(obj);
            //smestu on click!!!!!
            str = `<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
                <img src="assets/img/slikeOglasa/${obj.advertId}/${mojiOglasi[id].naziviSlikaOglasa[0]}">
            </div>
            <div class="col-sm ">
                <h5>${obj.nazivOglasa}</h5>
                <p onclick="prikazinfoOKorisniku(${e})" style="cursor:pointer;">Zahteva: ${obj.imeZahtevaoca}</p>
                <p>Datum: ${vreme[0]}</p>
                <p>Vreme: ${vreme[1]}</p>
            </div>
            <div id="zakazanOtkazan" class="col-sm d-flex align-items-center justify-content-end odgovor">
                <h5 onclick="potvrda('DA',${obj.advertId},${obj.userId},${obj.id})" style="color:#40b30e; cursor:pointer">Potvrdi</h5>&nbsp; &nbsp;
                <h5 onclick="potvrda('NE',${obj.advertId},${obj.userId},${obj.id})" style="color: #bd1b13; cursor:pointer">Odbij</h5>
            </div>
            <hr class="mt-4 mb-4">  
          </div>`;
        }
        else if(obj.response=="DA")
        {
            str = `<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
            <img src="assets/img/slikeOglasa/${obj.advertId}/${mojiOglasi[id].naziviSlikaOglasa[0]}">
            </div>
            <div class="col-sm ">
            <h5>${obj.nazivOglasa}</h5>
            <p>Zahteva: ${obj.imeZahtevaoca}</p>
            <p>Datum: ${vreme[0]}</p>
            <p>Vreme: ${vreme[1]}</p>
        </div>
        <div id="zakazanOtkazan" class="col-sm d-flex align-items-center justify-content-end odgovor">
            Zakazano razgledanje
        </div>
        <hr class="mt-4 mb-4">  
      </div>`;
        }
        else if(obj.response=="NE")
        {
            str = `<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
            <img src="assets/img/slikeOglasa/${obj.advertId}/${mojiOglasi[id].naziviSlikaOglasa[0]}">
            </div>
            <div class="col-sm ">
            <h5>${obj.nazivOglasa}</h5>
            <p>Zahteva: ${obj.imeZahtevaoca}</p>
            <p>Datum: ${vreme[0]}</p>
            <p>Vreme: ${vreme[1]}</p>
        </div>
        <div id="zakazanOtkazan" class="col-sm d-flex align-items-center justify-content-end odgovor">
            Odbijeno razgledanje
        </div>
        <hr class="mt-4 mb-4">  
      </div>`;
        }
        
        elementi.innerHTML += str;
    }
}


//korisnik vidi od korisnika
function prikazinfoOKorisniku(i)
{
    prebaciMeNaInformacijeOKorisniku(zahtevaju[i]);
}

function prebaciMeNaInformacijeOKorisniku(informacije)
{
    // informacije=JSON.stringify(informacije);
	// console.log(informacije);
    // console.log(informacije.userId);
    sessionStorage.setItem("idKorisnikaKogGledas",informacije.userId);
	window.location.pathname="/Front/infoOKorisnikuAdmin.html";
}



function potvrda(potrvrdjeno,advertId,userId,notificationId)
{
    var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    var theUrl = "http://localhost:8081/notifications/response";
    xmlhttp.open("POST", theUrl, true);

    var objekatZaSlanje = new Object();
    objekatZaSlanje.response=potrvrdjeno;
    objekatZaSlanje.advertId=advertId;
    objekatZaSlanje.userId=userId;
    objekatZaSlanje.id=notificationId;

    //console.log(JSON.stringify(params));
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (xmlhttp.responseText == "zakazano") {
                document.querySelector("#zakazanOtkazan").innerHTML="Zakazano razgledanje";
            }
            else
            {
                document.querySelector("#zakazanOtkazan").innerHTML="Odbijeno razgledanje";
            }
            loadZahtevaju();
        }
    }
    xmlhttp.send(JSON.stringify(objekatZaSlanje));
}
function prikazMojihOglasa() {
    elementi.innerHTML = "";
    for (e in mojiOglasi) {
        obj = mojiOglasi[e];
        console.log(obj);
        // console.log(obj.naziviSlikaOglasa);
        if(obj.naziviSlikaOglasa===null)
        {
            str = `<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
                <img src="assets/img/slikeOglasa/NoImageFound.jpg">
            </div>
            <div class="col-sm ">
                <h5 onclick="otvoriOglas(${obj.advertId})">${obj.naziv}</h5>
                <p>Grad: ${obj.grad}</p>
                <p>Deo grada: ${obj.deoGrada}</p>
                <p>Adresa: ${obj.ulica}</p>
            </div>
            <div class="col-sm d-flex align-items-center justify-content-end odgovor">
                <h5 style="cursor:pointer" onclick="obrisiISetuj(${obj.advertId})">Obrisi oglas</h5>
            </div>
            <hr class="mt-4 mb-4">  
        </div>`;
        }
        else
        { 
        str = `<div class="row pt-1 sredi">
        <div class="d-inline-flex" style="width: 220px; height:150px;">
            <img src="assets/img/slikeOglasa/${obj.advertId}/${obj.naziviSlikaOglasa[0]}">
        </div>
        <div class="col-sm ">
            <h5 onclick="otvoriOglas(${obj.advertId})">${obj.naziv}</h5>
            <p>Grad: ${obj.grad}</p>
            <p>Deo grada: ${obj.deoGrada}</p>
            <p>Adresa: ${obj.ulica}</p>
        </div>
        <div class="col-sm d-flex align-items-center justify-content-end odgovor">
            <h5 style="cursor:pointer" onclick="obrisiISetuj(${obj.advertId})">Obrisi oglas</h5>
        </div>
        <hr class="mt-4 mb-4">  
    </div>`;
        }
        elementi.innerHTML += str;
    }
}
// <h5 style="cursor:pointer" onclick="obrisiOglas(${obj.advertId})">Obrisi oglas</h5>
function obrisiISetuj(x)
{
    obrisiOglas(x);
    alertNotifikacijaZaObrisanOglas();
    setujPrikaz();
}

var oglas;

function otvoriOglas(id) {
    //console.log(id);

    for (let j = 0; j < mojiOglasi.length; j++)
        if (mojiOglasi[j].advertId == id)
            oglas = mojiOglasi[j];
    sessionStorage.removeItem("oglas");
    sessionStorage.setItem("oglas", JSON.stringify(oglas));
    window.open("./oglas.html");
}

function obrisiOglas(id) {
    var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    var theUrl = "http://localhost:8081/adverts/advertDBDelete";
    xmlhttp.open("POST", theUrl, true);

    var fd = new FormData();
    fd.append("advertId", id);

    //console.log(JSON.stringify(params));
    //xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (xmlhttp.responseText == "uspesno") {
                setujPrikaz();
                loadMojiOglasi();
                // alertNotifikacijaZaObrisanOglas();
                
            }
        }
    }
    xmlhttp.send(fd);
}
ime = document.querySelector("#ime");
prezime = document.querySelector("#prezime");
broj_telefona = document.querySelector("#broj_telefona");
email = document.querySelector("#email");
sifra = document.querySelector("#sifra");
dugmePotvrdi = document.querySelector("#dugmePotvrdi");

function dispMojeInformacije() {
    document.querySelector("#IME").innerHTML = mojeInformacije.ime;
    document.querySelector("#PREZIME").innerHTML = mojeInformacije.prezime;;
    document.querySelector("#BROJ_TELEFONA").innerHTML = mojeInformacije.brTelefona;
    document.querySelector("#EMAIL").innerHTML = mojeInformacije.mail;
    document.querySelector("#SIFRA").innerHTML = mojeInformacije.sifra;
    if (!mojeInformacije.slika.match("prazno.jpg")) {
        console.log("USAO");
        str = `assets/img/slikeKorisnika/${mojeInformacije.mail}/${mojeInformacije.slika}`;
        // console.log(str);
        document.querySelector("#slika").src = str;
    }
    document.querySelector("#imeSilka").innerHTML = mojeInformacije.ime + " " + mojeInformacije.prezime;
}

// izmeni podatke setovanje
var indZaMenjanje = -1;
izmeniPodatke = document.querySelector("#izmeniPodatke");

function setujMenjanje() {
    console.log("USO");
    indZaMenjanje *= -1;
    if (indZaMenjanje > 0) {
        ime.style = `display:"visible"`;
        prezime.style = `display:"visible"`;
        broj_telefona.style = `display:"visible"`;
        email.style = `display:"visible"`;
        sifra.style = `display:"visible"`;
        dugmePotvrdi.style = `display:"visible"`;
    } else {
        ime.style.display = `none`;
        prezime.style.display = `none`;
        broj_telefona.style.display = `none`;
        email.style.display = `none`;
        sifra.style.display = `none`;
        dugmePotvrdi.style.display = `none`;
    }
}

function prikupiSalji() {


    var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    var theUrl = "http://localhost:8081/api/updateUser";
    xmlhttp.open("POST", theUrl, true);

    var params = new Object();
    if (ime.value == "")
        params.ime = mojeInformacije.ime;
    else
        params.ime = ime.value;
    if (prezime.value == "")
        params.prezime = mojeInformacije.prezime;
    else
        params.prezime = prezime.value;
    if (broj_telefona.value == "")
        params.brTelefona = mojeInformacije.brTelefona;
    else
        params.brTelefona = broj_telefona.value;
    if (email.value == "")
        params.mail = mojeInformacije.mail;
    else
        params.mail = email.value;
    if (sifra.value == "")
        params.sifra = mojeInformacije.sifra;
    else
        params.sifra = sifra.value;
    params.id = sessionStorage.getItem("idKorisnika");
    console.log(JSON.stringify(params));
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp.onreadystatechange = function() { //Call a function when the state changes.
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (xmlhttp.responseText == "uspesno") {
                loadMojeInfo();
            }
        }
    }
    xmlhttp.send(JSON.stringify(params));
}


indPrikaza = -1;

function setIndPrikaza(x) {
    if (x.textContent == "Notifikacije")
        indPrikaza = -1;
    else if (x.textContent == "Moji oglasi")
        indPrikaza = 1;

    setujPrikaz();
}

function setujPrikaz() {
    // NOTIFIKACIJE
    console.log(indPrikaza);
    if (indPrikaza < 0) {
        
        document.querySelector("#notifikacije").style.color = "#ffb727";
        document.querySelector("#mojOglas").style.color = "";
        document.querySelector("#notifikacije").style.cursor="pointer";
        document.querySelector("#mojOglas").style.cursor="pointer";
        // console.log("boja:notifikacije");
        prikazZahteva();
        ispisOdgovoraNaZahtev();
    } else {
        
        document.querySelector("#mojOglas").style.color = "#ffb727";
        document.querySelector("#notifikacije").style.color = "";
        document.querySelector("#notifikacije").style.cursor="pointer";
        document.querySelector("#mojOglas").style.cursor="pointer";
        console.log("boja:mojiOglasi");
        prikazMojihOglasa();
    }
}
function alertNotifikacijaZaObrisanOglas()
{
    if(window.location.pathname!="/Front/infoOKorisnikuAdmin.html")
    {
        document.querySelector("#alert").style="display:visible";
        setujPrikaz();
    }
    else
    {
        document.querySelector("#alert").style="display:visible";
    }
    
}