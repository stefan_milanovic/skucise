const nam=document.querySelector("form.register #ime");
const lastname=document.querySelector("form.register #prezime");
const number=document.querySelector("form.register #broj");
const em=document.querySelector("form.register #email");
const loz=document.querySelector("form.register #lozinka");
const pon=document.querySelector("form.register #ponovi");
const slika=document.querySelector("form.register #ponovi");

let profilna=null;
let mail=null;

const imeHelp=document.querySelector('#imeHelp');
const prezimeHelp = document.querySelector('#prezimeHelp');
const brojHelp = document.querySelector('#brojHelp');
const emailHelp = document.querySelector('#emailHelp');
const lozinkaHelp = document.querySelector('#lozinkaHelp');
const ponovljenaHelp = document.querySelector('#ponovljenaHelp');
const slikaHelp = document.querySelector('#slikaHelp');

loz.addEventListener("input", function(){
    pon.pattern=`${this.value}`;
    
})

nam.addEventListener("keyup", function(){
    if(nam.checkValidity())
        imeHelp.style.display="none";
    else
        imeHelp.style.display="block";
    
})

lastname.addEventListener("keyup", function(){
    if(lastname.checkValidity())
        prezimeHelp.style.display="none";
    else
        prezimeHelp.style.display="block";
    
})

number.addEventListener("keyup", function(){
    if(number.checkValidity())
        brojHelp.style.display="none";
    else
        brojHelp.style.display="block";
    
})
em.addEventListener("keyup", function(){
    if(em.checkValidity())
        emailHelp.style.display="none";
    else
        emailHelp.style.display="block";
    
})

loz.addEventListener("keyup", function(){
    if(loz.checkValidity())
        lozinkaHelp.style.display="none";
    else
        lozinkaHelp.style.display="block";
    
})
pon.addEventListener("keyup", function(){
    if(pon.checkValidity())
        ponovljenaHelp.style.display="none";
    else
        ponovljenaHelp.style.display="block";
    
})






function registrujSe()
{
    const ime=document.getElementById("ime").value;
    const prezime=document.getElementById("prezime").value;
    const broj=document.getElementById("broj").value;
    const email=document.getElementById("email").value;
    mail=email;
    const lozinka=document.getElementById("lozinka").value;
    if(nam.checkValidity() && lastname.checkValidity() && number.checkValidity() && em.checkValidity() && loz.checkValidity() && pon.checkValidity())
    {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        var theUrl = "http://localhost:8081/api/m2";
        xmlhttp.open("POST", theUrl,true);

        var params=new Object();
        params.ime=ime;
        params.prezime=prezime;
        params.brTelefona=broj;
        params.mail=email;
        params.sifra=lozinka;
        params.brojOglasa=0;
        params.prosecnaOcena=0.0;
        console.log(JSON.stringify(params));
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if(xmlhttp.responseText=="uspesno")
                {
                    posaljiSliku();
                    predji();
                }
                else if(xmlhttp.responseText=="postoji")
                {
                    emailHelp.innerHTML="Korisnik sa unetim Email-om vec postoji!";
                    emailHelp.style.display="block";
                }
                else
                {
                    alert("Greska pri povezivanju sa serverom!");
                }
                   
            }
        }

        xmlhttp.send(JSON.stringify(params));
    }
    
    

}
function predji()
{
    window.open("./uspesnaRegistracija.html");
    window.close();
}
function proba(){
    let httpRequest = new XMLHttpRequest();
    httpRequest.open("GET","http://localhost:8081/api/m1");

    
    httpRequest.send();
    httpRequest.onload=function(){
        alert(httpRequest.responseText);
    }

}
function posaljiSliku()
{
    if(profilna!=null)
    {
        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
        var theUrl = "http://localhost:8081/api/upload";
        xmlhttp.open("POST", theUrl,true);

        let fd = new FormData;
        fd.append("file",profilna);
        fd.append("mail",mail);

        xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
       
            }
        }
        xmlhttp.send(fd);
    }
        

}
const imgDiv=document.querySelector('.profile-pic-div');
const img = document.querySelector('#profilna');
const file = document.querySelector('#file');
const uploadBtn = document.querySelector('#uploadBtn');

imgDiv.addEventListener('mouseenter',function(){
    uploadBtn.style.display="block";
});
imgDiv.addEventListener('mouseleave',function(){
    uploadBtn.style.display="none";
});

file.addEventListener('change',function(){
    profilna = this.files[0];
    
    if(profilna){
        const reader = new FileReader();

        reader.addEventListener('load',function(){
            img.setAttribute('src',reader.result);
        });
        reader.readAsDataURL(profilna);
    }
});
