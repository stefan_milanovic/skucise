var mojiOglasi;
var salji;
var infoOvogKorisnika;

function loadZaAdmina() {
    if (sessionStorage.infoOKorisnikuAdmin) {
        informacije = sessionStorage.infoOKorisnikuAdmin;
        informacije = JSON.parse(informacije);
        infoOvogKorisnika = informacije;
        console.log(informacije);
        document.querySelector("#naziv").innerHTML = informacije.ime + " " + informacije.prezime;
        document.querySelector("#imeSilka").innerHTML = informacije.ime + " " + informacije.prezime;
        document.querySelector("#IME").innerHTML = informacije.ime;
        document.querySelector("#PREZIME").innerHTML = informacije.prezime;;
        document.querySelector("#BROJ_TELEFONA").innerHTML = informacije.brTelefona;
        document.querySelector("#EMAIL").innerHTML = informacije.mail;
        if (!informacije.slika.match("prazno.jpg")) {
            console.log("USAO");
            str = `assets/img/slikeKorisnika/${informacije.mail}/${informacije.slika}`;
            // console.log(str);
            document.querySelector("#slika").src = str;
        }
        // document.querySelector("#imeSilka").innerHTML = mojeInformacije.ime+" "+mojeInformacije.prezime;

        document.querySelector("#notifikacije").innerHTML = "Oglasi korisnika";
        document.querySelector("#mojOglas").style.display = "none";
        //poziv funkcije za loudovanje
        console.log("louduje");
        loadOglasiKorisnika();
    } else {
        console.log(sessionStorage.idKorisnikaKogGledas);
        id = sessionStorage.idKorisnikaKogGledas;
        salji = new Object();
        salji.id = parseInt(id);


        var xmlhttp2 = new XMLHttpRequest();
        var theUrl = "http://localhost:8081/api/userInformation";
        xmlhttp2.open("POST", theUrl, true);

        xmlhttp2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");



        xmlhttp2.onreadystatechange = function() {
            if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
                info = xmlhttp2.responseText
                console.log(xmlhttp2.responseText);
                info = JSON.parse(xmlhttp2.responseText);

                ime = document.querySelector("#IME");
                ime.innerHTML = info.ime;

                prezime = document.querySelector("#PREZIME");
                prezime.innerHTML = info.prezime;
                document.querySelector("#naziv").innerHTML = `Korisnik: ${ime.innerHTML} ${prezime.innerHTML}`;
                document.querySelector("#BROJ_TELEFONA").innerHTML = info.brTelefona;
                document.querySelector("#EMAIL").innerHTML = info.mail;
                document.querySelector("#slika").src = `assets/img/slikeKorisnika/${info.mail}/${info.slika}`;
                document.querySelector("#imeSilka").innerHTML = ime.innerHTML + " " + prezime.innerHTML;
            }
        }
        console.log(salji);
        salji = JSON.stringify(salji);
        console.log(salji);
        xmlhttp2.send(salji);
        document.querySelector("#notifikacije").style.display = "none";
        document.querySelector("#mojOglas").style.display = "none";
    }
}

oglasiKorisnika = null;
if (sessionStorage.mail)
    salji.mail = sessionStorage.mail;
else
    salji.mail = sessionStorage.admin;


var mojiOglasi;

function loadOglasiKorisnika() {
    var xmlhttp2 = new XMLHttpRequest();
    var theUrl = "http://localhost:8081/adverts/myadvert";
    xmlhttp2.open("POST", theUrl, true);

    xmlhttp2.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xmlhttp2.onreadystatechange = function() {
        if (xmlhttp2.readyState == 4 && xmlhttp2.status == 200) {
            console.log(xmlhttp2.responseText);
            if (xmlhttp2.responseText.length > 0) {
                mojiOglasi = JSON.parse(xmlhttp2.responseText);
                console.log(oglasiKorisnika);
                prikazOglasa();
            }
        }
    }
    q = new Object();
    console.log(infoOvogKorisnika);
    q.mail = infoOvogKorisnika.mail;
    console.log(q);
    q = JSON.stringify(q);
    xmlhttp2.send(q);
}

function prikazOglasa() {
    elementi = document.querySelector("#elementi");
    elementi.innerHTML = "";
    for (e in mojiOglasi) {
        obj = mojiOglasi[e];
        console.log(obj);
        // console.log(obj.naziviSlikaOglasa);
        if (obj.naziviSlikaOglasa === null) {
            str = `<div class="row pt-1 sredi">
            <div class="d-inline-flex" style="width: 220px; height:150px;">
                <img src="assets/img/slikeOglasa/NoImageFound.jpg">
            </div>
            <div class="col-sm ">
                <h5 onclick="otvoriOglas(${obj.advertId})">${obj.naziv}</h5>
                <p>Grad: ${obj.grad}</p>
                <p>Deo grada: ${obj.deoGrada}</p>
                <p>Adresa: ${obj.ulica}</p>
            </div>
            <div class="col-sm d-flex align-items-center justify-content-end odgovor">
                <h5 style="cursor:pointer" onclick="obrisiISetuj(${obj.advertId})">Obrisi oglas</h5>
            </div>
            <hr class="mt-4 mb-4">  
        </div>`;
        } else {
            str = `<div class="row pt-1 sredi">
        <div class="d-inline-flex" style="width: 220px; height:150px;">
            <img src="assets/img/slikeOglasa/${obj.advertId}/${obj.naziviSlikaOglasa[0]}">
        </div>
        <div class="col-sm ">
            <h5 onclick="otvoriOglas(${obj.advertId})">${obj.naziv}</h5>
            <p>Grad: ${obj.grad}</p>
            <p>Deo grada: ${obj.deoGrada}</p>
            <p>Adresa: ${obj.ulica}</p>
        </div>
        <div class="col-sm d-flex align-items-center justify-content-end odgovor">
            <h5 style="cursor:pointer" onclick="obrisiISetuj(${obj.advertId})">Obrisi oglas</h5>
        </div>
        <hr class="mt-4 mb-4">  
    </div>`;
        }
        elementi.innerHTML += str;
    }

}