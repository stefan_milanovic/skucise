/**
* Template Name: Laura - v4.7.0
* Template URL: https://bootstrapmade.com/laura-free-creative-bootstrap-theme/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
*/

// PLACE FOR ADDEVENTLISTENER
if(window.location.pathname.match("/index.html"))
{
  window.addEventListener('scroll',function()
  {
    if(this.pageYOffset > 500 && this.location.pathname=="/index.html")
    {
      this.document.querySelector("#pocetna").classList="nav-link scrollto";
      this.document.querySelector("#ponuda").classList="nav-link scrollto active";
      this.document.querySelector("header").style=`background-color: black;`
    }
    else
    {
      this.document.querySelector("#pocetna").classList="nav-link scrollto active";
      this.document.querySelector("#ponuda").classList="nav-link scrollto ";
      this.document.querySelector("header").style=``;
    }
  });
}


(function() {
  "use strict";

  /**
   * Easy selector helper function
   */
  const select = (el, all = false) => {
    el = el.trim()
    if (all) {
      return [...document.querySelectorAll(el)]
    } else {
      return document.querySelector(el)
    }
  }

  /**
   * Easy event listener function
   */
  const on = (type, el, listener, all = false) => {
    let selectEl = select(el, all)
    if (selectEl) {
      if (all) {
        selectEl.forEach(e => e.addEventListener(type, listener))
      } else {
        selectEl.addEventListener(type, listener)
      }
    }
  }

  /**
   * Easy on scroll event listener 
   */
  const onscroll = (el, listener) => {
    el.addEventListener('scroll', listener)
  }

  /**
   * Navbar links active state on scroll
   */
  let navbarlinks = select('#navbar .scrollto', true)
  const navbarlinksActive = () => {
    let position = window.scrollY + 200
    navbarlinks.forEach(navbarlink => {
      if (!navbarlink.hash) return
      let section = select(navbarlink.hash)
      if (!section) return
      if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
        navbarlink.classList.add('active')
      } else {
        navbarlink.classList.remove('active')
      }
    })
  }
  window.addEventListener('load', navbarlinksActive)
  onscroll(document, navbarlinksActive)

  /**
   * Scrolls to an element with header offset
   */
  const scrollto = (el) => {
    let header = select('#header')
    let offset = header.offsetHeight

    if (!header.classList.contains('header-scrolled')) {
      offset -= 20
    }

    let elementPos = select(el).offsetTop
    window.scrollTo({
      top: elementPos - offset,
      behavior: 'smooth'
    })
  }

  /**
   * Toggle .header-scrolled class to #header when page is scrolled
   */
  let selectHeader = select('#header')
  if (selectHeader) {
    const headerScrolled = () => {
      if (window.scrollY > 100) {
        selectHeader.classList.add('header-scrolled')
      } else {
        selectHeader.classList.remove('header-scrolled')
      }
    }
    window.addEventListener('load', headerScrolled)
    onscroll(document, headerScrolled)
  }

  /**
   * Back to top button
   */
  let backtotop = select('.back-to-top')
  if (backtotop) {
    const toggleBacktotop = () => {
      if (window.scrollY > 100) {
        backtotop.classList.add('active')
      } else {
        backtotop.classList.remove('active')
      }
    }
    window.addEventListener('load', toggleBacktotop)
    onscroll(document, toggleBacktotop)
  }

  /**
   * Mobile nav toggle
   */
  on('click', '.mobile-nav-toggle', function(e) {
    select('#navbar').classList.toggle('navbar-mobile')
    this.classList.toggle('bi-list')
    this.classList.toggle('bi-x')
  })

  /**
   * Mobile nav dropdowns activate
   */
  on('click', '.navbar .dropdown > a', function(e) {
    if (select('#navbar').classList.contains('navbar-mobile')) {
      e.preventDefault()
      this.nextElementSibling.classList.toggle('dropdown-active')
    }
  }, true)

  /**
   * Scrool with ofset on links with a class name .scrollto
   */
  on('click', '.scrollto', function(e) {
    if (select(this.hash)) {
      e.preventDefault()

      let navbar = select('#navbar')
      if (navbar.classList.contains('navbar-mobile')) {
        navbar.classList.remove('navbar-mobile')
        let navbarToggle = select('.mobile-nav-toggle')
        navbarToggle.classList.toggle('bi-list')
        navbarToggle.classList.toggle('bi-x')
      }
      scrollto(this.hash)
    }
  }, true)

  /**
   * Scroll with ofset on page load with hash links in the url
   */
  window.addEventListener('load', () => {
    if (window.location.hash) {
      if (select(window.location.hash)) {
        scrollto(window.location.hash)
      }
    }
  });

  /**
   * Skills animation
   */
  let skilsContent = select('.skills-content');
  if (skilsContent) {
    new Waypoint({
      element: skilsContent,
      offset: '80%',
      handler: function(direction) {
        let progress = select('.progress .progress-bar', true);
        progress.forEach((el) => {
          el.style.width = el.getAttribute('aria-valuenow') + '%'
        });
      }
    })
  }

  /**
   * Testimonials slider
   */
  new Swiper('.testimonials-slider', {
    speed: 600,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

  /**
   * Porfolio isotope and filter
   */
  window.addEventListener('load', () => {
    let portfolioContainer = select('.portfolio-container');
    if (portfolioContainer) {
      let portfolioIsotope = new Isotope(portfolioContainer, {
        itemSelector: '.portfolio-item'
      });

      let portfolioFilters = select('#portfolio-flters li', true);

      on('click', '#portfolio-flters li', function(e) {
        e.preventDefault();
        portfolioFilters.forEach(function(el) {
          el.classList.remove('filter-active');
        });
        this.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: this.getAttribute('data-filter')
        });

      }, true);
    }

  });

  /**
   * Initiate portfolio lightbox 
   */
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });

  /**
   * Portfolio details slider
   */
  new Swiper('.portfolio-details-slider', {
    speed: 400,
    loop: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    }
  });

})()

function siteAlert()
{
  alert("Trenutno nije u funkciji!");
  return false;
}

function prijavljenKorisnik()
{
  const logovanje = document.querySelector('#logovanje');
  const registracija = document.querySelector('#registracija');
  var email=sessionStorage.getItem("mail");
  if(email!=null)
  {
    logovanje.innerHTML="Dodaj oglas";
    logovanje.href="./dodajOglas.html";
    registracija.innerHTML="Moj nalog";
    registracija.href="./mojNalog.html";
  }
}
//Prikaz ulogovanog/izlogovanog korisnika
function prijavljenKorisnikV2()
{
  var email = sessionStorage.getItem("mail");
  var admin = sessionStorage.getItem("admin");
  // console.log(email);
  // console.log(admin);
  if(email!=null)
  {
    document.querySelector("#logovanje").style.display="none";
    document.querySelector('#registracija').style.display="none"
    document.querySelector("#dodajOglas").style.display="visible";
    document.querySelector("#mojNalog").style.display="visible";
    document.querySelector("#korisnici").style.display="none";
    document.querySelector("#nalog").style.display="visible";
  }
  else if(admin!=null)
  {
    document.querySelector("#logovanje").style.display="none";
    document.querySelector('#registracija').style.display="none"
    document.querySelector("#dodajOglas").style.display="visible";
    document.querySelector("#mojNalog").style.display="visible";
    document.querySelector("#korisnici").style.display="visible";
    document.querySelector("#nalog").style.display="visible";
  }
  else
  {
    document.querySelector("#logovanje").style.display="visible";
    document.querySelector('#registracija').style.display="visible"
    document.querySelector("#dodajOglas").style.display="none";
    document.querySelector("#mojNalog").style.display="none";
    document.querySelector("#korisnici").style.display="none";
    document.querySelector("#nalog").style.display="none";
  }
}
function logout()
{
  sessionStorage.removeItem("mail");
  sessionStorage.removeItem("admin");
  window.location="./index.html";
}

// Gradovi
function infoGradovi()
{
  let httpRequest = new XMLHttpRequest();

    gradovi=new Set();
    deloviGrada=new Set();

    httpRequest.open("GET","http://localhost:8081/adverts/cities");

    
    httpRequest.send();
    httpRequest.onload=function()
    {
        // console.log(httpRequest.responseText);
        adv = httpRequest.responseText;
        adv=JSON.parse(adv);
        
        
        gradoviIzbor=document.querySelector("#gradoviIzbor");
        // console.log(gradoviIzbor);
        
        deloviGradaIzbor=document.querySelector("#deloviiGradaIzbor");

        for(el in adv)
        {
          // console.log(adv[el].grad);
          // console.log(adv[el].deoGrada);
          gradovi.add(adv[el].grad);
          deloviGrada.add(adv[el].deoGrada);
          deloviGradaIzbor.disabled="disable";
        }
        // let add=`<option value="" disabled selected hidden>Gradovi</option>`;
//prikaz na index.html 
        if(window.location.pathname.match("/index.html"))
        {
          add=`<option selected value="-">Svi gradovi</option>`;
          for (item of gradovi.values())
          {
            // console.log(item);
            add+=`<option value="${item}">${item}</option>`;
          }
        }
        else if(window.location.pathname.match("/dodajOglas.html"))
        {
          add = `<option value="" disabled selected hidden>Gradovi</option>`;
          for (item of gradovi.values())
          {
            add+=`<option value="${item}">${item}</option>`;
          }
          add+=`<option value="novGrad">Drugi grad..</option>`;
        }

        gradoviIzbor.innerHTML = add;

//delovi grada
        document.querySelector("#gradoviIzbor").addEventListener('change',function()
        {
          add=`<option value="" disabled selected hidden>Delovi grada</option>`;
          if(this.value != "-")
          {

            if(window.location.pathname.match("/index.html"))
            {
              deloviGradaIzbor.disabled="";
              add = `<option selected value="-">Svi delovi grada</option>`;
              skup = new Set();
              for (item of adv.values())
              {
                
                if(this.value == item.grad)
                {
                  skup.add(item.deoGrada);
                  console.log("RADIM");
                  // add+=`<option value="${item.deoGrada}">${item.deoGrada}</option>`;
                }
              }
              // console.log(skup);
              for(item of skup)
              {
                console.log(item);
                 add+=`<option value="${item}">${item}</option>`;
              }
              deloviGradaIzbor.innerHTML = add; 
            }
            else if (window.location.pathname.match("/dodajOglas.html"))
            {
              add = `<option disabled selected value="-" hidden>Svi delovi grada</option>`;
              skup = new Set();
              for(item of adv.values())
              {
                if(this.value == item.grad)
                {
                  // add+=`<option value="${item.deoGrada}">${item.deoGrada}</option>`;
                  skup.add(item.deoGrada);
                }
              }
              console.log(skup);
              for(item of skup)
              {
                add+=`<option value="${item}">${item}</option>`;
              }
              add+=`<option value="novDeoGrada">Drugi deo grada..</option>`
               deloviGradaIzbor.innerHTML = add;
            }
            // deloviGradaIzbor.innerHTML = add;
          } 
          else
          {
            deloviGradaIzbor.disabled="disable";
            deloviGradaIzbor.innerHTML = `<option value="" disabled selected hidden>Delovi grada</option>`;
            
          }
        }
        );
      
    }
}

// Grejanja
function infoGrejanja()
{
  heatingAdd=document.querySelector("#heatingAdd");
  let httpRequest = new XMLHttpRequest();
  httpRequest.open("GET","http://localhost:8081/adverts/heating");
  httpRequest.send();
  str="";
  httpRequest.onload=function(){
    heating=httpRequest.responseText;
    heating = JSON.parse(heating);
    str="";
    heating.forEach(element=>
      str+=`<input type="checkbox" class="c1" value=${element}>
            <label>${element}</label><br>
            ` 
      );

    // console.log(str);
    heatingAdd.innerHTML = str;
  }

  if(window.location.pathname!="/Front/index.html")
    document.querySelector("#minS").value=2.5;
  else
  {
    // document.querySelector("#minS").value="Od
    // document.querySelector("#maxS").value="Do:";
  }
    

}
